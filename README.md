# Voices for Children

## Setup

Make sure node > 10 is installed.

Install node modules:

```
npm i
```

## Development

Start the gatsby development environment:

```
npm start
```

This will start the site on <http://localhost:8000> and the GraphQL server on <http://localhost:8000/___graphql>.
