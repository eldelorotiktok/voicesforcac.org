import 'typeface-roboto'
import 'typeface-cabin'
import 'typeface-permanent-marker'

export const onRouteUpdate = () => {
  if (process.env.NODE_ENV === `production` && typeof fbq === `function`) {
    fbq('track', 'PageView')
  }
}

export const onServiceWorkerUpdateReady = () => {
  if (typeof window !== 'undefined' && window) {
    window.location.reload()
  }
}
