const path = require('path')
const { createFilePath } = require('gatsby-source-filesystem')
const { fmImagesToRelative } = require('gatsby-remark-relative-images')

const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin')

function isIndexPage(post) {
  return path.basename(post.node.fileAbsolutePath) === 'index.md'
}

exports.onCreateWebpackConfig = ({ actions }) => {
  actions.setWebpackConfig({
    resolve: {
      plugins: [new TsconfigPathsPlugin()],
    },
  })
}

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions

  return new Promise((resolve, reject) => {
    const templates = {
      single: path.resolve('./src/templates/page.tsx'),
      blocks: path.resolve('./src/templates/blocks-page.tsx'),
      page_with_blocks: path.resolve('./src/templates/page-with-blocks.tsx'),
      event: path.resolve('./src/templates/event-page.tsx'),
      events: path.resolve('./src/templates/event-list-page.tsx'),
    }
    resolve(
      // query for markdown files
      graphql(
        `
          {
            allMarkdownRemark(
              sort: { fields: [frontmatter___title], order: ASC }
              limit: 1000
            ) {
              edges {
                node {
                  excerpt(format: PLAIN)
                  fileAbsolutePath
                  fields {
                    slug
                  }
                  frontmatter {
                    title
                    template
                  }
                }
              }
            }
          }
        `
      ).then(result => {
        if (result.errors) {
          console.log(result.errors)
          reject(result.errors)
        }

        // create the posts
        const posts = result.data.allMarkdownRemark.edges

        for (post of posts) {
          let postContext = {
            slug: post.node.fields.slug,
          }

          // if it's a post index, add corresponding posts to context
          if (isIndexPage(post)) {
            const parentRelPath = path.dirname(
              path.relative(__dirname, post.node.fileAbsolutePath)
            )
            postContext.children = posts.filter(({ node }) => {
              if (path.basename(node.fileAbsolutePath) === 'index.md') {
                return false
              }
              const childRelPath = path.relative(
                __dirname,
                node.fileAbsolutePath
              )
              return childRelPath.startsWith(parentRelPath)
            })
          }

          // determine which layout to use for post
          let templateComponent
          if (post.node.frontmatter.template) {
            templateComponent = templates[post.node.frontmatter.template]
          } else {
            templateComponent = templates.single
          }

          createPage({
            path: post.node.fields.slug,
            component: templateComponent,
            context: postContext,
          })
        }
      })
    )
  })
}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions

  fmImagesToRelative(node)

  if (node.internal.type === 'MarkdownRemark' || node.internal.type === 'Mdx') {
    const value = createFilePath({ node, getNode })
    createNodeField({
      name: `slug`,
      node,
      value,
    })
  }
}

exports.createSchemaCustomization = ({ actions }) => {
  const { createTypes } = actions

  const typeDefs = `
  type MarkdownRemarkFrontmatterBlocksCardsFooterItems {
    link: String
    style: String
    text: String
  }
    type MarkdownRemarkFrontmatterBlocksCards {
      footer_items: MarkdownRemarkFrontmatterBlocksCardsFooterItems
      header: String
      image: File @fileByRelativePath
      link: String
    }

    type MarkdownRemarkFrontmatterBlocksImage {
      image: File @fileByRelativePath
    }

    type MarkdownRemarkFrontmatterBlocks {
      background_image: File @fileByRelativePath
      background_image_opacity: String
      cards: [MarkdownRemarkFrontmatterBlocksCards]
      image: File @fileByRelativePath
      images: [MarkdownRemarkFrontmatterBlocksImage]
    }

    type MarkdownRemarkFrontmatter {
      background_image: File @fileByRelativePath
      blocks: [MarkdownRemarkFrontmatterBlocks]
      image: File @fileByRelativePath
      title_background_image: File @fileByRelativePath
      title_background_image_opacity: String
    }

    type MarkdownRemark implements Node {
      frontmatter: MarkdownRemarkFrontmatter
    }
  `
  createTypes(typeDefs)
}
