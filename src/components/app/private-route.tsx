import React from 'react'
import PropTypes from 'prop-types'
import { navigate } from 'gatsby'

import { isLoggedIn } from '../../services/auth-service'

const PrivateRoute = ({ component: Component, ...rest }) => {
  if (!isLoggedIn() && window.location.pathname !== `/private`) {
    navigate(`/app`)
    return null
  }

  return <Component {...rest} />
}

PrivateRoute.propTypes = {
  component: PropTypes.any.isRequired,
}

export default PrivateRoute
