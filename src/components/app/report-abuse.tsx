import React from 'react'
import styled from 'styled-components'
import { useStaticQuery, graphql } from 'gatsby'

import PageTitle from '../page-title'
import { sizes } from '../../breakpoints'
import SEO from '../seo'

const StyledIframe = styled.iframe`
  width: 100%;
  height: 6700px;
  @media (min-width: ${sizes.mobile}) {
    height: 6200px;
  }
`

export const ReportAbuse = () => {
  const data = useStaticQuery(graphql`
    {
      fileName: file(
        relativePath: { eq: "sharon-mccutcheon-Vl0KHsz67kE-unsplash.jpg" }
      ) {
        childImageSharp {
          fluid(maxWidth: 1600) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)
  return (
    <>
      <SEO title="Report Abuse" />
      <PageTitle
        title="Report Abuse"
        title_background="Purple"
        title_background_image={data.fileName}
        title_background_image_opacity={0.5}
      />
      <section className="section page-content">
        <main className="container content-container">
          <div className="content">
            <StyledIframe
              src="https://docs.google.com/forms/d/e/1FAIpQLSdqb7YspAOeLd82BYVow9h5z3j0cZGpUxtWhvfgLBCgkmPm5w/viewform?embedded=true"
              frameBorder="0"
              marginWidth="0"
              marginHeight="0"
              title="Report Abouse Form"
            >
              Loading&#8230;
            </StyledIframe>
          </div>
        </main>
      </section>
    </>
  )
}

export default ReportAbuse
