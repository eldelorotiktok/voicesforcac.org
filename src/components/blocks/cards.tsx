import React from 'react'
import Card from '../card'

const Cards = ({ block }) => {
  if (!block.cards || !block.cards.length) {
    return <></>
  }
  return (
    <div className="section">
      <div className="container">
        <div className="columns is-multiline is-centered">
          {block.cards.map((item, idx) => {
            const {
              content,
              footer_items: footerItems,
              header,
              image,
              link,
              subtitle,
              title,
              title_top: titleTop,
            } = item
            const cardData = {
              content,
              footerItems,
              header,
              image,
              link,
              subtitle,
              title,
              titleTop,
            }
            return (
              <div key={`card-${idx}`} className="column is-4">
                <Card cardData={cardData} />
              </div>
            )
          })}
        </div>
      </div>
    </div>
  )
}

export default Cards
