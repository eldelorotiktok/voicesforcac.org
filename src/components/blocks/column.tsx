import React from 'react'
import Markdown from '../markdown'

const Column = ({ block }) => {
  let className = 'column'
  switch (block.width) {
    case 'One Half':
      className += ' is-half'
      break
    case 'One Third':
      className += ' is-one-third'
      break
    case 'Two Thirds':
      className += ' is-two-thirds'
      break
    case 'One Quarter':
      className += ' is-one-quarter'
      break
    default:
      break
  }
  return (
    <div className={className}>
      <Markdown alignment={block.alignment} content={block.content} />
    </div>
  )
}

export default Column
