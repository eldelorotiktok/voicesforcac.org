import React from 'react'
import Markdown from '../markdown'

const Content = ({ block }) => (
  <div className="section">
    <div className="container">
      <Markdown alignment={block.alignment} content={block.content} />
    </div>
  </div>
)

export default Content
