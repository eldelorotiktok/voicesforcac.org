import React from 'react'

import Link from '../link'
import { getBackgroundClass } from '../../helpers/lib/get-background-class'
import { StyledSection, BackgroundImage, Title, HeroBody } from './hero'

const CTA = ({ block }) => {
  const sectionClassName = block.background
    ? `hero is-medium has-text-centered is-${getBackgroundClass(
        block.background
      )}`
    : 'hero is-medium has-text-centered'
  return (
    <StyledSection className={sectionClassName} background={block.background}>
      {block.background_image && (
        <BackgroundImage
          background_image={block.background_image}
          background_image_opacity={block.background_image_opacity}
        />
      )}
      <HeroBody
        className="hero-body"
        title={block.title}
        subtitle={block.subtitle}
      >
        <div className="container">
          {block.title && <Title className="title is-1">{block.title}</Title>}
          {block.subtitle && <p className="subtitle">{block.subtitle}</p>}
          {block.button.url && block.button.text && (
            <Link
              className={
                !block.background || block.background === 'Light'
                  ? 'button is-primary is-large'
                  : 'button is-primary is-large is-inverted'
              }
              to={block.button.url}
            >
              {block.button.text}
            </Link>
          )}
        </div>
      </HeroBody>
    </StyledSection>
  )
}

export default CTA
