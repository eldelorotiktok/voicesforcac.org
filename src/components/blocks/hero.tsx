import React from 'react'
import styled, { css } from 'styled-components'

import { getBackgroundColor } from '../../helpers/lib/get-background-color'
import { getBackgroundClass } from '../../helpers/lib/get-background-class'
import { breakpoints } from '../../breakpoints'

export const StyledSection = styled.section`
  position: relative;
  background-color: ${({ background, theme }) =>
    getBackgroundColor(background, theme)} !important;
`

export const BackgroundImage = styled.div`
  ${({ background_image }) =>
    background_image
      ? css`
          background-image: url(${background_image.childImageSharp.fluid.src});
        `
      : ''};

  background-repeat: no-repeat;
  background-position: 50% 50%;
  background-size: cover;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  opacity: ${({ background_image_opacity }) => background_image_opacity || 1};
`

export const Title = styled.h1`
  font-family: ${({ theme }) => theme.familyTertiary} !important;
`

export const HeroBody = styled.div`
  @media ${breakpoints.tablet} {
    ${props =>
      !props.title && !props.subtitle
        ? css`
            padding-bottom: 12rem !important;
            padding-top: 12rem !important;
          `
        : ''}
  }
`

const Hero = ({ block }) => {
  const sectionClassName = block.background
    ? `hero is-medium has-text-centered is-${getBackgroundClass(
        block.background
      )}`
    : 'hero is-medium has-text-centered'
  return (
    <StyledSection className={sectionClassName} background={block.background}>
      {block.background_image && (
        <BackgroundImage
          background_image={block.background_image}
          background_image_opacity={block.background_image_opacity}
        />
      )}
      <HeroBody
        className="hero-body"
        title={block.title}
        subtitle={block.subtitle}
      >
        <div className="container">
          {block.title && <Title className="title is-1">{block.title}</Title>}
          {block.subtitle && <p className="subtitle is-4">{block.subtitle}</p>}
        </div>
      </HeroBody>
    </StyledSection>
  )
}

export default Hero
