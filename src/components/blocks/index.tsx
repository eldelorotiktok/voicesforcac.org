import React from 'react'

import ThreeCol from './3col'
import Feature from './feature'
import CTA from './cta'
import Hero from './hero'
import Content from './content'
import Column from './column'
import Columns from './columns'
import Map from './map'
import Cards from './cards'
import Banner from './banner'
import Gallery from './gallery'

const Blocks = props => {
  const { blocks } = props

  return (
    <>
      {blocks.map((block, idx) => {
        switch (block.component) {
          case '3col':
            return <ThreeCol key={idx} block={block} />
          case 'feature':
            return <Feature key={idx} block={block} />
          case 'cta':
            return <CTA key={idx} block={block} />
          case 'hero':
            return <Hero key={idx} block={block} />
          case 'content':
            return <Content key={idx} block={block} />
          case 'column':
            return <Column key={idx} block={block} />
          case 'columns':
            return <Columns key={idx} block={block} />
          case 'map':
            return <Map key={idx} block={block} />
          case 'cards':
            return <Cards key={idx} block={block} />
          case 'banner':
            return <Banner key={idx} block={block} />
          case 'gallery':
            return <Gallery key={idx} block={block} />
          default:
            return ''
        }
      })}
    </>
  )
}

export default Blocks
