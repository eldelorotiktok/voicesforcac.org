import React from 'react'
import styled from 'styled-components'

import MapComponent from '../map'

const StyledMap = styled.div`
  /* margin: 3rem 0; */
`

const Map = ({ block }) => {
  const { latitude, longitude } = block
  const markers = [block]
  return (
    <StyledMap>
      <div style={{ height: 400 }}>
        <MapComponent
          latitude={Number(latitude)}
          longitude={Number(longitude)}
          markers={markers}
        />
      </div>
    </StyledMap>
  )
}

export default Map
