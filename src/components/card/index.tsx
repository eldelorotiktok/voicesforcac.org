import React, { CSSProperties } from 'react'
import Img from 'gatsby-image'
import styled from 'styled-components'

import Link from '../link'

const generateKey = pre => {
  return `${pre}-${new Date().getTime()}`
}

const TitleTop = ({ className, titleTop }) => {
  return <small className={className}>{titleTop}</small>
}

const StyledLink = styled(Link)`
  color: #4a4a4a;
  &:hover {
    color: #4a4a4a;
  }
`

const StyledTitleTop = styled(TitleTop)`
  display: block;
  margin-bottom: 1rem;
`

const StyledCardHeader = styled.header``

const CardHeader = ({ header }) => {
  return (
    <StyledCardHeader className="card-header">
      <div className="card-header-title">{header}</div>
    </StyledCardHeader>
  )
}

const CardImage = ({ image, title }) => {
  return (
    <div className="card-image">
      <figure className="image">
        <Img fluid={image.childImageSharp.fluid} alt={title} title={title} />
      </figure>
    </div>
  )
}

const CardContent = ({ cardData }) => {
  const { content, subtitle, title, titleTop } = cardData
  return (
    <>
      {(titleTop || title || subtitle || content) && (
        <div className="card-content">
          {titleTop && <StyledTitleTop titleTop={titleTop} />}
          {title && <p className="title is-4">{title}</p>}
          {subtitle && <p className="subtitle is-6">{subtitle}</p>}
          {content && (
            <div
              className="content"
              dangerouslySetInnerHTML={{ __html: content }}
            />
          )}
        </div>
      )}
    </>
  )
}

const StyledCardFooter = styled.footer`
  margin-top: auto;
  .card-footer-item {
    a {
      margin: 0 0.5rem;
    }
  }
`

const CardFooter = ({ footerItems }) => {
  return (
    <StyledCardFooter className="card-footer">
      <div className="card-footer-item">
        {footerItems.map((footerItem, idx) => (
          <Link
            key={generateKey(`card-footer-item-${idx}`)}
            to={footerItem.link}
            className={`button is-${footerItem.style}`}
          >
            {footerItem.text}
          </Link>
        ))}
      </div>
    </StyledCardFooter>
  )
}

const style: CSSProperties = {
  display: 'flex',
  flexDirection: 'column',
  height: '100%',
}

const Card = ({ cardData }) => {
  const { footerItems, header, image, link, title } = cardData
  if (link) {
    return (
      <div className="card" style={style}>
        <StyledLink to={link}>
          {header && <CardHeader header={header} />}
          {image && <CardImage image={image} title={title} />}
          <CardContent cardData={cardData} />
        </StyledLink>
        {footerItems && footerItems.length > 0 && (
          <CardFooter footerItems={footerItems} />
        )}
      </div>
    )
  }
  return (
    <div className="card" style={style}>
      {header && <CardHeader header={header} />}
      {image && <CardImage image={image} title={title} />}
      <CardContent cardData={cardData} />
      {footerItems && footerItems.length > 0 && (
        <CardFooter footerItems={footerItems} />
      )}
    </div>
  )
}

export default Card
