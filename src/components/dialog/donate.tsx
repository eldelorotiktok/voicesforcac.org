import React from 'react'
import Dialog from '@material-ui/core/Dialog'
import styled from 'styled-components'

import { breakpoints } from 'src/breakpoints'

const Modal = styled.div`
  .modal-content {
    margin: 0;
    width: 100%;
    .title {
      color: inherit;
    }
    > p {
      margin-bottom: 1rem;
      &:last-of-type {
        margin-bottom: 0;
      }
    }
  }
  .dialog-footer {
    button {
      margin-right: 0.5rem;
      &:last-of-type {
        margin-right: 0;
      }
    }
  }
`

const DonationInfo = styled.div`
  align-items: center;
  background: ${({ theme }) => theme.primary};
  color: ${({ theme }) => theme.light};
  display: flex;
  flex-flow: row wrap;
  justify-content: center;
  margin-bottom: 1rem;
  padding: 1rem 1rem;
  text-align: center;
  @media ${breakpoints.desktop} {
    justify-content: flex-start;
  }
  .title {
    margin-bottom: 0;
    margin-right: 1rem;
    &:last-of-type {
      margin-right: 0;
    }
  }
`

const Donate = props => {
  const { onClose, data, open } = props
  const { amount, customAmount, duration } = data
  const donationAmount = amount || customAmount

  const handleClose = () => {
    onClose()
  }

  const confirm = () => {
    onClose('donate')
  }

  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby="donate-dialog-title"
      open={open}
    >
      <Modal>
        <div className="dialog-title" id="donate-dialog-title">
          Donation confirmation
        </div>
        <div className="dialog-content">
          <div className="modal-content">
            <p className="is-size-4">
              Thank you for supporting Voices for Children.
            </p>
            <DonationInfo>
              <div className="title is-1">${donationAmount}</div>
              <div className="title is-3">
                {duration === 'monthly'
                  ? 'Monthly Contribution'
                  : 'One Time Donation'}
              </div>
            </DonationInfo>

            <p className="has-text-weight-light">
              A {duration === 'once' ? 'one time' : 'monthly'} ${donationAmount}{' '}
              charge will processed using your supplied payment information.
            </p>
          </div>
        </div>
        <div className="dialog-footer">
          <button className="button is-primary" onClick={() => confirm()}>
            Donate
          </button>
          <button
            className="button is-primary is-outlined"
            onClick={() => handleClose()}
          >
            Cancel
          </button>
        </div>
      </Modal>
    </Dialog>
  )
}

export default Donate
