import React from 'react'
import Dialog from '@material-ui/core/Dialog'
import styled from 'styled-components'

const Modal = styled.div`
  .modal-content {
    margin: 0;
    width: 100%;
    .title {
      color: inherit;
    }
    > p {
      margin-bottom: 1rem;
      &:last-of-type {
        margin-bottom: 0;
      }
    }
  }
  .dialog-footer {
    button {
      margin-right: 0.5rem;
      &:last-of-type {
        margin-right: 0;
      }
    }
  }
`

const DonationCancellation = props => {
  const { onClose, open } = props

  const handleClose = () => {
    onClose()
  }

  const confirm = () => {
    onClose()
  }

  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby="donation-cancellation-dialog-title"
      open={open}
    >
      <Modal>
        <div className="dialog-title" id="donation-cancellation-dialog-title">
          Donation cancellation request
        </div>
        <div className="dialog-content">
          <div className="modal-content">
            <p>
              (add form here - Survey Gizmo? custom form using SendGrid,
              etc...?)
            </p>
          </div>
        </div>
        <div className="dialog-footer">
          <button className="button is-primary" onClick={() => confirm()}>
            Submit
          </button>
          <button
            className="button is-primary is-outlined"
            onClick={() => handleClose()}
          >
            Cancel
          </button>
        </div>
      </Modal>
    </Dialog>
  )
}

export default DonationCancellation
