import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons'

const MoneyInput = ({
  label,
  name,
  placeholder,
  register,
  required,
  error,
  onChange,
}) => (
  <div className="field">
    <label className="label">{label}</label>
    <div className="control">
      <div className="field has-addons">
        <div className="control">
          <div className="button is-static">$</div>
        </div>
        <div className="control is-expanded has-icons-right">
          <input
            type="number"
            className={`input ${error ? 'is-danger' : ''}`}
            name={name}
            placeholder={placeholder}
            ref={register({ required })}
            min={0}
            onChange={onChange}
          />
          {error && (
            <span className="icon is-small is-right has-text-danger">
              <FontAwesomeIcon icon={faExclamationTriangle} />
            </span>
          )}
        </div>
      </div>
    </div>
    {error && <div className="help is-danger">{error}</div>}
  </div>
)

export default MoneyInput
