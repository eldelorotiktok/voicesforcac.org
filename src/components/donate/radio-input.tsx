import React from 'react'
import styled from 'styled-components'
import { breakpoints } from '../../breakpoints'

export const RadioGroup = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
  flex-flow: row wrap;
  margin-top: -1rem;

  .radio-container {
    margin-top: 1rem;
    position: relative;
    width: calc(50% - 0.5rem);
    &:last-of-type {
      width: 100%;
    }
    @media ${breakpoints.desktop} {
      width: calc(20% - 0.5rem);
      &:last-of-type {
        width: calc(20% - 0.5rem);
      }
    }
  }

  .radio-button {
    opacity: 0;
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    margin: 0;
    cursor: pointer;
  }

  .radio-tile {
    background: transparent;
    border: 1px solid ${({ theme }) => theme.primary};
    color: ${({ theme }) => theme.primary};
    display: flex;
    flex-direction: column;
    font-family: ${({ theme }) => theme.familySecondary};
    font-weight: bold;
    font-size: 1.5rem;
    align-items: center;
    justify-content: center;
    width: 100%;
    height: 100%;
    border-radius: 4px;
    padding: 0.5rem 1rem;
    transition: transform 300ms ease;
  }

  .radio-button:hover,
  .radio-button:checked {
    + .radio-tile {
      background: ${({ theme }) => theme.primary};
      border: 1px solid transparent;
      color: ${({ theme }) => theme.light};
    }
  }
`

const RadioInput = ({ label, name, options, register, onChange, required }) => (
  <div className="field">
    <label className="label">{label}</label>
    <RadioGroup className="control">
      {options.map((option, idx) => (
        <label className="radio-container" key={`amount-${idx}`}>
          <input
            className="radio-button"
            type="radio"
            name={name}
            value={option.value}
            ref={register}
            onChange={onChange}
            required={required}
          />
          <div className="radio-tile">{option.label}</div>
        </label>
      ))}
    </RadioGroup>
  </div>
)

export default RadioInput
