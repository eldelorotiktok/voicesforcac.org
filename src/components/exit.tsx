import React, { useState } from 'react'
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import Tour from 'reactour'

const ExitButton = styled.div`
  position: fixed;
  z-index: 10000;
  right: 20px;
  bottom: 20px;
`

const exitContent = ({ close }) => {
  return (
    <div>
      <p className="mb-2">
        This button will take you quickly away from our page if someone is
        nearby and you don&apos;t want them to see.
      </p>
      <p className="mb-2">
        This won&apos;t clear your browser history or prevent this site from
        appearing on monitoring apps.
      </p>
      <p className="mb-2">
        If you&apos;re afraid of someone discovering this page give us a call
        instead when you are safe and free.
      </p>
      <p className="mb-2">
        Call 911 if you&apos;re in a dangerous situation and need immediate
        help.
      </p>
      <button id="exitButton" className="button is-primary" onClick={close}>
        Got it!
      </button>
    </div>
  )
}

const tourConfig = [
  {
    selector: '#exitButton',
    content: exitContent,
    position: 'top',
    style: { maxWidth: 500 },
  },
]

const seeYaSites = [
  'https://www.google.com',
  'https://weather.com/',
  'https://www.bing.com/',
  'https://www.yahoo.com/',
]

const shuffleArray = arr => {
  for (let i = arr.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1))
    const temp = arr[i]
    arr[i] = arr[j]
    arr[j] = temp
  }
  return arr
}

const Exit = () => {
  const [open, setOpen] = useState(
    Boolean(sessionStorage.getItem('tourShown') !== 'true')
    // Boolean(localStorage.getItem('tourShown') !== 'true')
  )

  const seeYa = () => {
    const list = shuffleArray(seeYaSites)
    location.replace(list[0])
  }

  const closeTour = () => {
    setOpen(false)
    sessionStorage.setItem('tourShown', 'true')
    // localStorage.setItem('tourShown', 'true')
  }

  return (
    <ExitButton>
      <Tour
        showNumber={false}
        showButtons={false}
        showNavigation={false}
        showCloseButton={false}
        onRequestClose={closeTour}
        steps={tourConfig}
        isOpen={open}
        maskClassName="tour-mask"
        className="helper"
        rounded={5}
      />
      <button id="exitButton" className="button is-primary" onClick={seeYa}>
        <span className="icon">
          <FontAwesomeIcon className="fa-lg" icon={faTimes} />
        </span>
      </button>
    </ExitButton>
  )
}

export default Exit
