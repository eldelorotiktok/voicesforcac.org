import React, { useState } from 'react'
import Carousel, { Modal, ModalGateway } from 'react-images'
import Img from 'gatsby-image'
import styled from 'styled-components'
import { breakpoints } from '../breakpoints'

const gutter = 10

const modalStyleFn = styleObj => ({ ...styleObj, zIndex: 100 })

const StyledImageGallery = styled.div`
  overflow: hidden;
  margin-left: -${gutter}px;
  margin-right: -${gutter}px;
`

const StyledImageGalleryImage = styled.div`
  background-color: #eee;
  box-sizing: border-box;
  cursor: pointer;
  float: left;
  margin: ${gutter}px;
  overflow: hidden;
  padding-bottom: 75%;
  position: relative;
  width: calc(100% - ${gutter * 2}px);

  @media ${breakpoints.desktop} {
    padding-bottom: 25%;
    width: calc(33% - ${gutter * 2}px);
  }

  /* transition: all 0.25s; */

  &:hover {
    opacity: 0.9;
    /* transform: scale(1.025, 1.025); */
  }
`

const Gallery = props => {
  const [state, setState] = useState({
    selectedIndex: 0,
    lightboxIsOpen: false,
  })

  const toggleLightbox = selectedIndex => {
    setState(state => ({
      lightboxIsOpen: !state.lightboxIsOpen,
      selectedIndex,
    }))
  }

  const closeLightbox = () => {
    toggleLightbox(null)
  }

  const { title, description, images } = props
  const { lightboxIsOpen, selectedIndex } = state
  if (!images || images.length === 0) {
    return <></>
  }
  const filteredImages = images.filter(img => !!img.image)
  const galleryImages = filteredImages.map(img => {
    return {
      caption: img.title,
      source: {
        download: img.image.childImageSharp.original.src,
        fullscreen: img.image.childImageSharp.original.src,
        regular: img.image.childImageSharp.fluid.src,
        thumbnail: img.image.childImageSharp.fixed.src,
      },
    }
  })
  return (
    <div>
      {title && <p className="title is-2">{title}</p>}
      {description && <p className="title is-4">{description}</p>}
      <StyledImageGallery>
        {filteredImages.map(({ image, title }, j) => (
          <StyledImageGalleryImage
            onClick={() => toggleLightbox(j)}
            key={`gallery-image-${j}-${image.childImageSharp.fixed.src}`}
          >
            <Img
              alt={title}
              title={title}
              fixed={image.childImageSharp.fixed}
              style={{
                position: 'absolute',
                height: '100%',
                width: '100%',
                top: -9999,
                bottom: -9999,
                left: -9999,
                right: -9999,
                margin: 'auto',
              }}
            />
          </StyledImageGalleryImage>
        ))}
      </StyledImageGallery>
      <ModalGateway>
        {lightboxIsOpen ? (
          <Modal
            onClose={() => closeLightbox()}
            styles={{ blanket: modalStyleFn, positioner: modalStyleFn }}
          >
            <Carousel currentIndex={selectedIndex} views={galleryImages} />
          </Modal>
        ) : null}
      </ModalGateway>
    </div>
  )
}

export default Gallery
