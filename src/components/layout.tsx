import React from 'react'
// import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { useStaticQuery, graphql } from 'gatsby'
import styled, { ThemeProvider } from 'styled-components'
import { breakpoints } from '../breakpoints'

import Header from './header'
import Footer from './footer'
import theme from '../theme'
// import { UpdateNotification } from '../components/notification/update-notification'

import '../scss/main.scss'

const StyledBody = styled.div`
  display: flex;
  min-height: 100vh;
  flex-direction: column;
  padding-top: 4.5rem;
  @media ${breakpoints.desktop} {
    padding-top: 6.5rem;
  }
`

const StyledChildren = styled.div`
  flex: 1;
`

const Layout = ({ children }) => {
  // const [show, setShow] = useState(true)
  const data = useStaticQuery(graphql`
    {
      site {
        ...SiteMetadata
      }
    }
  `)
  return (
    <ThemeProvider theme={theme}>
      <StyledBody>
        <Header />
        <StyledChildren>{children}</StyledChildren>
        <Footer content={data.site.siteMetadata.footer} />
        {/* <UpdateNotification state={{ show, setShow }} /> */}
      </StyledBody>
    </ThemeProvider>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
