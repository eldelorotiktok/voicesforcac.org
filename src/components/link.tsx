import React from 'react'
import { Link as GatsbyLink } from 'gatsby'

const ALink = ({ to, children, innerRef, ...other }) => (
  <a href={to} ref={innerRef} {...other}>
    {children}
  </a>
)

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const Link = React.forwardRef<any, any>(function Link(props, ref) {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const { to, activeClassName, partiallyActive, ...other } = props as any
  const internal = /^((\.\.\/)+|\/)(?!\/)/.test(to)

  if (internal) {
    const file = /\.[0-9a-z]+$/i.test(to)
    if (file) {
      // Get rid of the relative links and the static path
      const newTo = to.replace(/(\.\.\/)+static\//g, '/')
      return <ALink href={newTo} innerRef={ref} {...other} />
    }
    return (
      <GatsbyLink
        to={to}
        activeClassName={activeClassName}
        partiallyActive={partiallyActive}
        innerRef={ref}
        {...other}
      />
    )
  }
  return <ALink href={to} innerRef={ref} {...other} />
})

Link.displayName = `Link`

export default Link
