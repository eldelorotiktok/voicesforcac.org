import React from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import Img from 'gatsby-image'

const Logo = () => {
  const data = useStaticQuery(graphql`
    {
      site {
        ...SiteMetadata
      }
      file(relativePath: { eq: "voices-for-children-logo.png" }) {
        childImageSharp {
          fluid(maxWidth: 108) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)
  return (
    <Img
      fluid={data.file.childImageSharp.fluid}
      alt={`${data.site.siteMetadata.title} Logo`}
      title={data.site.siteMetadata.title}
    />
  )
}

export default Logo
