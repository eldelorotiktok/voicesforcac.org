import React from 'react'

import Markdown from '../markdown'

const Popup = props => {
  const { content } = props
  return <Markdown className="content" content={content} />
}

export default Popup
