import React from 'react'
import { markdownToHtml } from '../helpers/markdown'

export interface MarkdownProps {
  alignment?: 'Left' | 'Center' | 'Right' | 'Justified'
  className?: string
  content: string
}

const Markdown = ({
  alignment,
  className,
  content,
  ...other
}: MarkdownProps) => {
  let newClassName = className ? `${className} content` : 'content'
  switch (alignment) {
    case 'Left':
      newClassName += ' has-text-left'
      break
    case 'Center':
      newClassName += ' has-text-centered'
      break
    case 'Right':
      newClassName += ' has-text-right'
      break
    case 'Justified':
      newClassName += ' has-text-justified'
      break
    default:
      break
  }
  return (
    <div className={newClassName} {...other}>
      {markdownToHtml(content)}
    </div>
  )
}

export default Markdown
