import React from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import styled from 'styled-components'
import Drawer from '@material-ui/core/Drawer'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeft, faAngleDown } from '@fortawesome/free-solid-svg-icons'
import ExpansionPanel from '@material-ui/core/ExpansionPanel'
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'
import { lighten } from 'polished'

import Link from '../link'
import Logo from '../logo'

const MobileMenuHeader = styled.div`
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 1rem;
  padding: 0.5rem;
`

const BackLink = styled.div`
  padding: 0.5rem;
`

const LogoLink = styled(Link)`
  display: block;
  padding: 0.5rem;
`
const LogoWrapper = styled.div`
  height: 28px;
  width: 54px;
`

const DonateLink = styled.div`
  margin-bottom: 1rem;
  padding-left: 1rem;
  padding-right: 1rem;
`

const MainMenu = styled.div``

const Hr = styled.hr`
  background: ${props => lighten(0.05, props.theme.dark)};
  margin-left: 1rem;
  margin-right: 1rem;
`

const SubMenu = styled.div``

const MenuItem = styled.div`
  /* margin-bottom: 0.5rem; */
  padding: 0 1rem;
  &:last-of-type {
    margin-bottom: 0;
  }
`

const MenuLink = styled(Link)`
  color: ${props => props.theme.light};
  display: block;
  font-weight: 700;
  padding: 0.5rem 0;
  width: 100%;
  &:hover,
  &:focus {
    color: ${props => props.theme.light};
  }
`

const SubMenuItem = styled(MenuItem)``

const SubMenuLink = styled(MenuLink)`
  font-weight: 400;
`

const ExpansionIcon = styled(FontAwesomeIcon)`
  color: ${props => props.theme.light};
`

const StyledMobileMenu = styled(Drawer)`
  .MuiPaper-root {
    background: transparent;
  }
  .MuiDrawer-paper {
    background: ${props => props.theme.dark};
    color: ${props => props.theme.light};
    width: 80%;
  }
  .MuiExpansionPanel-root {
    transition: all 0.25s ease-in-out;
    &.Mui-expanded {
      background: ${props => props.theme.secondary};
      margin: 0;
    }
    &:before {
      display: none;
    }
  }
  .MuiExpansionPanelSummary-root {
    min-height: unset;
    padding: 0 1rem;
    &.Mui-expanded {
      min-height: unset;
    }
    &.Mui-focused {
      background: ${props => lighten(0.05, props.theme.dark)};
    }
  }
  .MuiExpansionPanelSummary-content {
    margin: 0;
    &.Mui-expanded {
      margin: 0;
    }
  }
  .MuiExpansionPanelDetails-root {
    display: block;
    font-weight: 400;
    padding: 0 0.5rem 0.5rem;
  }
  .MuiPaper-elevation1 {
    box-shadow: none;
  }
  .MuiIconButton-root {
    padding: 0.25rem 0.5rem;
  }
  .MuiIconButton-edgeEnd {
    margin-left: 1rem;
    margin-right: -0.5rem;
  }
`

const MobileMenu = props => {
  const { state, toggleNavState } = props
  const data = useStaticQuery(graphql`
    fragment Submenu on DataJsonMenu_itemsSubmenu {
      title
      url
      type
      style
      open_in_a_new_window
    }

    fragment MenuItems on DataJsonMenu_items {
      title
      url
      type
      style
      open_in_a_new_window
      submenu {
        ...Submenu
      }
    }
    {
      allDataJson {
        edges {
          node {
            id
            menu_items {
              ...MenuItems
            }
          }
        }
      }
    }
  `)
  const leftNav = data.allDataJson.edges.filter(
    edge => edge.node.id === 'left_nav'
  )[0]
  const rightNav = data.allDataJson.edges.filter(
    edge => edge.node.id === 'right_nav'
  )[0]
  const donateNavitems = rightNav.node.menu_items.filter(
    item => item.url === '/donate'
  )
  const rightNavitems = rightNav.node.menu_items.filter(
    item => item.url !== '/donate'
  )

  const stopAndToggle = event => {
    event.stopPropagation()
    console.log('event.type', event.type)
    if (state.navbarOpen && event.type === 'click') {
      toggleNavState()
    }
  }

  return (
    <StyledMobileMenu open={state.navbarOpen} onClose={toggleNavState}>
      <MobileMenuHeader>
        <BackLink>
          <FontAwesomeIcon icon={faArrowLeft} onClick={toggleNavState} />
        </BackLink>
        <LogoLink to={'/'} onClick={toggleNavState}>
          <LogoWrapper>
            <Logo />
          </LogoWrapper>
        </LogoLink>
      </MobileMenuHeader>
      {donateNavitems &&
        donateNavitems.length > 0 &&
        donateNavitems.map((item, idx) => (
          <DonateLink key={idx}>
            <Link className="button is-primary is-fullwidth" to={item.url}>
              {item.title}
            </Link>
          </DonateLink>
        ))}
      <MainMenu>
        {leftNav.node.menu_items &&
          leftNav.node.menu_items.length > 0 &&
          leftNav.node.menu_items.map((item, idx) => {
            if (item.submenu && item.submenu.length) {
              return (
                <ExpansionPanel key={idx}>
                  <ExpansionPanelSummary
                    expandIcon={<ExpansionIcon icon={faAngleDown} />}
                    aria-label="Expand"
                  >
                    <MenuLink
                      to={item.url}
                      onClick={event => stopAndToggle(event)}
                      onFocus={event => stopAndToggle(event)}
                    >
                      {item.title}
                    </MenuLink>
                  </ExpansionPanelSummary>
                  <ExpansionPanelDetails>
                    {item.submenu.map((subItem, subIdx) => (
                      <SubMenuItem key={`${idx}-${subIdx}`}>
                        <SubMenuLink to={subItem.url} onClick={toggleNavState}>
                          {subItem.title}
                        </SubMenuLink>
                      </SubMenuItem>
                    ))}
                  </ExpansionPanelDetails>
                </ExpansionPanel>
              )
            }
            return (
              <MenuItem key={idx}>
                <MenuLink to={item.url} onClick={toggleNavState}>
                  {item.title}
                </MenuLink>
              </MenuItem>
            )
          })}
      </MainMenu>
      <Hr />
      <SubMenu>
        {rightNavitems &&
          rightNavitems.length > 0 &&
          rightNavitems.map((item, idx) => {
            if (item.submenu && item.submenu.length) {
              return (
                <ExpansionPanel key={idx}>
                  <ExpansionPanelSummary
                    expandIcon={<ExpansionIcon icon={faAngleDown} />}
                    aria-label="Expand"
                  >
                    <SubMenuLink
                      to={item.url}
                      onClick={event => stopAndToggle(event)}
                    >
                      {item.title}
                    </SubMenuLink>
                  </ExpansionPanelSummary>
                  <ExpansionPanelDetails>
                    {item.submenu.map((subItem, subIdx) => (
                      <SubMenuItem key={`${idx}-${subIdx}`}>
                        <SubMenuLink to={subItem.url} onClick={toggleNavState}>
                          {subItem.title}
                        </SubMenuLink>
                      </SubMenuItem>
                    ))}
                  </ExpansionPanelDetails>
                </ExpansionPanel>
              )
            }
            return (
              <MenuItem key={idx}>
                <MenuLink to={item.url} onClick={toggleNavState}>
                  {item.title}
                </MenuLink>
              </MenuItem>
            )
          })}
      </SubMenu>
    </StyledMobileMenu>
  )
}

export default MobileMenu
