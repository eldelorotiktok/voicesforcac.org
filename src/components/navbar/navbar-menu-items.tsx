import React from 'react'

import Link from '../link'

const NavbarMenuItems = ({ menuItems }) => {
  return menuItems.map((item, idx) => {
    if (item.submenu && item.submenu.length) {
      return (
        <div key={idx} className="navbar-item has-dropdown is-hoverable">
          <Link to={item.url} className="navbar-link">
            {item.title}
          </Link>

          <div className="navbar-dropdown">
            <NavbarMenuItems menuItems={item.submenu} />
          </div>
        </div>
      )
    }
    if (item.style) {
      return (
        <div key={idx} className="navbar-item">
          <div className="buttons">
            <Link to={item.url} className={`button is-${item.style}`}>
              {item.title}
            </Link>
          </div>
        </div>
      )
    }
    return (
      <Link key={idx} to={item.url} className="navbar-item">
        {item.title}
      </Link>
    )
  })
}

export default NavbarMenuItems
