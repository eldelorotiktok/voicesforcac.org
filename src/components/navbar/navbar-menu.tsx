import React from 'react'
import { useStaticQuery, graphql } from 'gatsby'

import NavbarMenuItems from './navbar-menu-items'

const NavbarMenu = props => {
  const data = useStaticQuery(graphql`
    fragment Submenu on DataJsonMenu_itemsSubmenu {
      title
      url
      type
      style
      open_in_a_new_window
    }

    fragment MenuItems on DataJsonMenu_items {
      title
      url
      type
      style
      open_in_a_new_window
      submenu {
        ...Submenu
      }
    }
    {
      allDataJson {
        edges {
          node {
            id
            menu_items {
              ...MenuItems
            }
          }
        }
      }
    }
  `)
  const leftNav = data.allDataJson.edges.filter(
    edge => edge.node.id === 'left_nav'
  )[0]
  const rightNav = data.allDataJson.edges.filter(
    edge => edge.node.id === 'right_nav'
  )[0]

  return (
    <div {...props}>
      <div className="navbar-start">
        <NavbarMenuItems menuItems={leftNav.node.menu_items} />
      </div>

      <div className="navbar-end">
        <NavbarMenuItems menuItems={rightNav.node.menu_items} />
      </div>
    </div>
  )
}

export default NavbarMenu
