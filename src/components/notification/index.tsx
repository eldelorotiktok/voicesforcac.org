import React from 'react'
import Snackbar from '@material-ui/core/Snackbar'

const Notification = ({
  state: { notificationState, setNotificationState },
}) => {
  const { open, message, type } = notificationState

  let className
  switch (type) {
    case 'error':
      className = 'is-danger'
      break
    case 'success':
      className = 'is-success'
      break
    default:
      className = 'is-primary'
  }
  const handleClose = () => {
    setNotificationState({ open: false })
  }

  return (
    <Snackbar
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      open={open}
      autoHideDuration={6000}
      onClose={handleClose}
    >
      <div className={`notification ${className}`}>
        <button className="delete" onClick={handleClose}></button>
        {message}
      </div>
    </Snackbar>
  )
}

export default Notification
