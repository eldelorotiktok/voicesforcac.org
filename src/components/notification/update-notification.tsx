import React from 'react'
import Snackbar from '@material-ui/core/Snackbar'
import styled from 'styled-components'

const Content = styled.div`
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
`

export const UpdateNotification = ({ state: { show, setShow } }) => {
  const handleClose = () => {
    setShow(false)
    if (typeof window !== 'undefined' && window) {
      window.location.reload()
    }
  }

  return (
    <Snackbar
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      open={show}
      onClose={handleClose}
    >
      <div className={`notification is-primary has-text-right pr-5`}>
        <Content>
          <div>There is a new version of this application ready.</div>
          <div className="ml-2">
            <button
              className="button is-primary is-inverted is-small"
              onClick={handleClose}
            >
              Reload
            </button>
          </div>
        </Content>
      </div>
    </Snackbar>
  )
}

export default UpdateNotification
