import React from 'react'
import styled from 'styled-components'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebookSquare } from '@fortawesome/free-brands-svg-icons'

import Link from '../../link'

const StyledLink = styled(Link)`
  color: ${props => props.theme.social.facebook};
  &:hover {
    color: ${props => props.theme.social.facebook};
    opacity: 0.9;
  }
`

const Facebook = ({ link }) => (
  <StyledLink to={link} title="Visit us on Facebook">
    <FontAwesomeIcon icon={faFacebookSquare} size="2x" />
  </StyledLink>
)

export default Facebook
