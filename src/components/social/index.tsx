import React from 'react'
import { useStaticQuery, graphql } from 'gatsby'

import Facebook from './facebook'

const Social = () => {
  const data = useStaticQuery(graphql`
    {
      site {
        ...SiteMetadata
      }
    }
  `)
  if (!data.site.siteMetadata.social.length) {
    return <></>
  }
  return (
    <div className="level">
      <div className="level-left"></div>
      <div className="level-right">
        {data.site.siteMetadata.social.map((item, idx) => {
          switch (item.type) {
            case 'Facebook':
              return (
                <div key={`social-facebook-${idx}`} className="level-item">
                  <Facebook link={item.link} />
                </div>
              )
            default:
              return ''
          }
        })}
      </div>
    </div>
  )
}

export default Social
