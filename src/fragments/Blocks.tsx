import { graphql } from 'gatsby'

export const Blocks = graphql`
  fragment Blocks on MarkdownRemarkFrontmatter {
    blocks {
      alignment
      background
      background_image {
        childImageSharp {
          fluid(maxWidth: 1600) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      background_image_opacity
      button {
        text
        url
      }
      cards {
        content
        footer_items {
          link
          style
          text
        }
        header
        image {
          childImageSharp {
            fluid(maxWidth: 800) {
              ...GatsbyImageSharpFluid
            }
          }
        }
        link
        subtitle
        title
        title_top
      }
      col1 {
        content
        title
      }
      col2 {
        content
        title
      }
      col3 {
        content
        title
      }
      columns {
        alignment
        content
        width
      }
      component
      content
      description
      featured_content
      image {
        childImageSharp {
          fluid(maxWidth: 1600) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      images {
        title
        image {
          childImageSharp {
            fixed(height: 600, width: 600, cropFocus: CENTER) {
              ...GatsbyImageSharpFixed
            }
            fluid(maxWidth: 1600) {
              ...GatsbyImageSharpFluid
            }
            original {
              src
            }
          }
        }
      }
      latitude
      longitude
      orientation
      popup_content
      subtitle
      title
    }
  }
`
