import { graphql } from 'gatsby'

export const BlocksFrontmatter = graphql`
  fragment BlocksFrontmatter on MarkdownRemark {
    ...Frontmatter
    frontmatter {
      ...Blocks
    }
  }
`
