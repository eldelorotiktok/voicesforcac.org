import { graphql } from 'gatsby'

export const EventFrontmatter = graphql`
  fragment EventFrontmatter on MarkdownRemark {
    ...Frontmatter
    frontmatter {
      address
      address_link
      ...Blocks
      end_date
      location
      start_date
    }
  }
`
