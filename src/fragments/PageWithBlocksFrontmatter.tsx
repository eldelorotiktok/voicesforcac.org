import { graphql } from 'gatsby'

export const PageWithBlocksFrontmatter = graphql`
  fragment PageWithBlocksFrontmatter on MarkdownRemark {
    ...Frontmatter
    frontmatter {
      ...Blocks
      position
    }
  }
`
