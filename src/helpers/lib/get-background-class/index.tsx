export const getBackgroundClass = backgroundColor => {
  switch (backgroundColor) {
    case 'Blue':
    case 'Purple':
    case 'Red':
    case 'Green':
    case 'Orange':
    case 'Light Blue':
    case 'Dark':
      return 'dark'
    case 'Light':
    default:
      return 'light'
  }
}
