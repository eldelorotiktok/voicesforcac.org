import moment from 'moment'

export const getMomentDateFormat = d => {
  if (d.hours() === 0 && d.minutes() === 0 && d.seconds() === 0) {
    return d.format('MMMM DD, YYYY')
  }
  return d.format('MMMM DD, YYYY h:mma')
}

export const getDateString = ({ start_date, end_date }) => {
  const startDate = moment(start_date)
  if (!startDate.isValid()) {
    return
  }
  const endDate = moment(end_date)
  if (endDate.isValid()) {
    if (!startDate.isSame(endDate)) {
      if (startDate.day() === endDate.day()) {
        return `${startDate.format('MMMM DD, YYYY')} ${startDate.format(
          'h:mma'
        )} - ${endDate.format('h:mma')}`
      }
      return `${getMomentDateFormat(startDate)} - ${getMomentDateFormat(
        endDate
      )}`
    }
  }
  return getMomentDateFormat(startDate)
}
