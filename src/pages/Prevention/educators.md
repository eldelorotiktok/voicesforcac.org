---
template: page_with_blocks
title_background: Light Blue
title_background_image_opacity: ".25"
position: Above
title: Educators
subtitle: ''
title_background_image: "/media/joshua-eckstein-VAJEea9u6k8-unsplash.jpg"
blocks:
- template: block__gallery
  component: gallery
  images:
  - title: Body Safety
    image: "/media/body-safety.jpg"
  - title: 'Darkness to Light: Stewards of Children'
    image: "/media/d2l-poster.jpg"
  - title: Online Safety Tips
    image: "/media/online-safety-tips-for-teens.jpg"
  - title: Social Media Safety Tips
    image: "/media/social-media-safety-tips.jpg"
  - title: When a Child Discloses
    image: "/media/when-a-child-discloses-flyer.jpg"
  - title: Raising Resilience Fighting ACEs
    image: "/media/raising-resilience-fighting-aces.jpg"
  title: ''
  description: ''

---
Welcome educators and service providers! Thank you for checking out our prevention page. Below you will find trainings, resources, and more on how you prevent adverse childhood experiences. [Learn more about reporting abuse & neglect here.](https://www.voicesforcac.org/resources/reporting)

## Child Abuse Prevention

Every adult who interacts with children, whether as a parent, teacher, neighbor, youth leader or volunteer should be speaking the same language when it comes to preventing child abuse and neglect. Can you imagine the strength and reassurance it would give a child to hear from every adult in their life about how important they are and that they deserve to be happy and safe?

Child abuse prevention can be a difficult conversation to have with children and even with teens. Voices for Children and our friendly and highly-trained staff are here to help. The Prevention and Community Education department has a curriculum for children and for adults.

Darkness to Light’s Stewards of Children Training is a sexual abuse prevention training program that will equip you to prevent, recognize, and react responsibly to child sexual abuse. and motivates you to courageous action. This training is now available virtually. Contact our [Prevention Education Coordinator](mailto:training@voicesforcac.org) to schedule a Darkness to Light training today!

## Body Safety for Kids: ROAR

Voices for Children uses the R.O.A.R Educational Program to teach children how to protect themselves from harm. R.O.A.R. stands for:

* **R**-emember privates are private
* **O**-kay to say no
* **A**-lways talk about secrets
* **R**-aise your voice and tell someone.

This 25-minute interactive training is available to K and 2nd-grade students in both Genesee and Shiawassee Counties. Training takes place in the classroom, with parental permission. Parent support is also provided as a part of the curriculum so adults from the family can participate and practice this invaluable lesson, using common language, with their child at home.

## Body Safety for Kids: VOICES lessons

Introducing our new Prevention Education Mini-Series entitled VOICES Lessons! These short educational videos are a resource for parents or providers to help teach children (ages 4-8) about body safety. Letting children know that their body belongs to them is an important part of preventing child sexual abuse. These videos are for a preschool/elementary-age audience and are best used in combination with guided discussion. We encourage parents or providers to preview the video to ensure you feel it is appropriate and then watch with your kids/ class as a conversation starter. When you talk, ask open-ended questions about what they thought of the video. Ask in a casual and warm tone to create an open dialogue.

<a href="https://www.voicesforcac.org/voices-lessons" class="button is-primary" target="_blank">VOICES Lesson's Videos</a>

## Keeping Children Safe Online

The internet is an important tool. Like many tools, when used correctly it is very helpful. However, many tools, when misused, can also create risks or dangers. The internet is no different. Adults can help reduce the risk for children who are using the internet by teaching them internet safety. Adults should also openly communicate about what to do when the child sees or reads something they don't understand and that might be upsetting.

Adults need to know the risks that exist online and teach children to make safe and responsible decisions. Voices for Children provides free social media safety trainings. The training covers online gaming, social media, apps, text-speak, human trafficking, sextortion, and more. Here are some practical things you can do to protect children online.

<a href="https://www.voicesforcac.org/training" class="button is-primary" target="_blank">See Our Available Trainings</a>

## Understanding the Impact of Childhood Trauma: Adverse Childhood Experiences (ACEs)

Adverse Childhood Experiences (ACEs) are traumatic events in a child's life that can have lasting effects into adulthood. Overcoming trauma is possible and is often connected to genuine, positive relationships. This is one of many trauma-informed practices you can use to promote resilience and improve the outcome of a child's life.

<a href="https://www.voicesforcac.org/training" class="button is-primary" target="_blank">See Our Available Trainings</a>

## Mandated Reporter Training

Michigan Child Protection Law mandates that certain professionals must report any suspicions of child abuse or neglect. Even if you are not mandated by law to report suspicions, each of us should feel ethically responsible to report any inclination that a child may be unsafe. Voices for Children offers individual and group [Mandated Reporter](https://www.michigan.gov/mdhhs/0,5885,7-339-73971_7119_50648_44443---,00.html) trainings that go over signs to look for in identifying abuse and neglect, the process of making a report, and other frequently asked questions. If you would like to host a training for your staff, community group, or to learn more as an individual, email [prevention@voicesforcac.org](mailto:prevention@voicesforcac.org).

## Professional Enrichment

Our staff train local professionals such as teachers, physicians, child care providers, and human service staff on various child welfare topics such as mandated reporter requirements, child sexual abuse prevention, infant safe sleep, shaken baby/abusive head trauma, and others.

### Human Trafficking Awareness

Human Trafficking: Human Trafficking is a hot topic in our media and society today, but this issue of individuals being exploited for the gain of another is an issue that our center deals with daily. Voices for Children is a part of the [Genesee County Human Trafficking Taskforce](http://www.gcohumantrafficking.org/). We provide education to parents and teens debunking myths and misconceptions surrounding the issue, identifying red flags to look for, and informing the community about how to report suspected human trafficking situations. To schedule a training, contact [prevention@voicesforcac.org](mailto:prevention@voicesforcac.org).