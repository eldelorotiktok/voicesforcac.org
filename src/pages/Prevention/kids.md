---
template: page_with_blocks
title_background: Light Blue
title_background_image_opacity: ".25"
position: Below
title: Kids
subtitle: ''
title_background_image: "/media/joshua-eckstein-VAJEea9u6k8-unsplash.jpg"
blocks:
- template: block__columns
  component: columns
  columns:
  - template: block__column
    component: column
    width: One Half
    alignment: Center
    content: "![](/media/rex-snippet.JPG)"
  - template: block__column
    component: column
    width: One Half
    alignment: Left
    content: |-
      # **R.O.A.R**

      ### **R** - Remember - Privates are Private!

      ### **O** - It's Okay to say NO!

      ### **A** - Always talk about secrets!

      ### **R** - Raise your voices and tell someone!
  title: ''
  description: ''

---
## Stay Safe Online

We know! You just can't live without your phones, tablets, gaming devices, and social media; we're not asking you to. All we ask is for you to be _smart_, be _fair_, be _safe_, and be _aware_. You are not immune to the risks of the internet. Know the dangers and the ways you can protect yourself. Talk with your parents about how to be safe online. If someone makes you feel uncomfortable or unsafe tell someone right away.

**Call 911 if you are in danger!**