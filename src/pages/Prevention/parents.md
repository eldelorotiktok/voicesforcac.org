---
template: page_with_blocks
title_background: Light Blue
title_background_image_opacity: ".25"
position: Above
title: Parents
subtitle: ''
title_background_image: "/media/joshua-eckstein-VAJEea9u6k8-unsplash.jpg"
blocks:
- template: block__gallery
  component: gallery
  images:
  - title: Body Safety
    image: "/media/body-safety.jpg"
  - title: Social Media Safety Tips
    image: "/media/social-media-safety-tips.jpg"
  - title: Online Safety Tips for Teens
    image: "/media/online-safety-tips-for-teens.jpg"
  - title: When a Child Discloses
    image: "/media/when-a-child-discloses-flyer.jpg"
  - title: 'Darkness to Light: Stewards of Children'
    image: "/media/d2l-poster.jpg"
  - title: Raising Resilience in Children
    image: "/media/raising-resilience-fighting-aces.jpg"
  title: ''
  description: ''

---
Welcome parents! Thank you for checking out our prevention page. Keeping our community's children safe is what drives us. We want to be an ally for you in keeping your own children safe too. Below you will find trainings, resources, and more on how you prevent adverse childhood experiences. [Learn more about reporting abuse & neglect here.](https://www.voicesforcac.org/resources/reporting)

## Child Abuse Prevention

Every adult who interacts with children, whether as a parent, teacher, neighbor, youth leader or volunteer should be speaking the same language when it comes to preventing child abuse and neglect. Can you imagine the strength and reassurance it would give a child to hear from every adult in their life about how important they are and that they deserve to be happy and safe?

Child abuse prevention can be a difficult conversation to have with children and even with teens. Voices for Children and our friendly and highly-trained staff are here to help. The Prevention and Community Education department has a curriculum for children and for adults.

Darkness to Light’s Stewards of Children Training is a sexual abuse prevention training program that will equip you to prevent, recognize, and react responsibly to child sexual abuse. and motivates you to courageous action. This training is now available virtually. Contact our [Prevention Education Coordinator](mailto:training@voicesforcac.org) to schedule a Darkness to Light training today!

## Body Safety for Kids: ROAR

Voices for Children uses the R.O.A.R Educational Program to teach children how to protect themselves from harm. R.O.A.R. stands for:

* **R**-emember privates are private
* **O**-kay to say no
* **A**-lways talk about secrets
* **R**-aise your voice and tell someone.

This 25-minute interactive training is available to K and 2nd-grade students in both Genesee and Shiawassee Counties. Training takes place in the classroom, with parental permission. Parent support is also provided as a part of the curriculum so adults from the family can participate and practice this invaluable lesson, using common language, with their child at home.

Our team of [Child & Family Therapists](https://www.voicesforcac.org/what-we-do/child-family-therapy/) are also available to work with families on talking to kids about body safety, boundaries, and healthy sexual development.

## Body Safety for Kids: VOICES lessons

Introducing our new Prevention Education Mini-Series entitled VOICES Lessons! These short educational videos are a resource for parents to help teach children (ages 4-8) about body safety. Letting children know that their body belongs to them is an important part of preventing child sexual abuse. These videos are for a preschool/elementary-age audience and are best used in combination with parental discussion. We encourage parents to preview the video to ensure you feel it is appropriate and then watch with your child as a conversation starter. When you talk, ask open-ended questions about what they thought of the video. Ask in a casual and warm tone to create an open dialogue.

<a href="https://www.voicesforcac.org/prevention/voices-lessons/" class="button is-primary" target="_blank">VOICES Lesson's Videos</a>

## Keeping Children Safe Online

The internet is an important tool. Like many tools, when used correctly it is very helpful. However, many tools, when misused, can also create risks or dangers. The internet is no different. Adults can help reduce the risk for children who are using the internet by teaching them internet safety. Adults should also openly communicate about what to do when the child sees or reads something they don't understand and that might be upsetting.

Adults need to know the risks that exist online and teach children to make safe and responsible decisions. Voices for Children provides free social media safety trainings. The training covers online gaming, social media, apps, text-speak, human trafficking, sextortion, and more. Here are some practical things you can do to protect children online.

<a href="https://www.voicesforcac.org/training" class="button is-primary" target="_blank">See Our Available Trainings</a>

## Local Leadership Group

Our Local Leadership Group works together with the home visiting programs of Genesee County and the Social-Emotional workgroup of the Great Start Collaborative. The purpose of this group is to enhance and provide better opportunities for home visiting programs and early childhood services.

The LLG promotes home visiting throughout the community as an essential and effective practice. The LLG also coordinates, guides, and advises countywide home visiting services and training opportunities for home visitors.

## Healthy Start Lead Expansion – Response to the Flint Water Crisis – Parent Group

Was your child exposed to Flint water during the height of the Flint Water Crisis? Are you concerned about the aftermath of this exposure and the effect it may or may not have on your children? The Genesee County Health Department Lead Exposure Access Program (LEAP) seeks to provide referrals to local resources for families that could help offset the risk of future complications due to exposure during the water crisis. As residents and parents, you are the experts surrounding what services and referrals have been effective and which have not. The LEAP Parent Advisory meets monthly to advise sub-recipient organizations of the Genesee County Health Department regarding the work that they are doing and the true needs of Flint families living through the water crisis. Participants are given opportunities for professional development training and supports to help them involve and refer other families to services and the help that they need surrounding the crisis. To learn more about the LEAP Parent Advisory Group, contact [alyssa@voicesforcac.org](mailto:alyssa@voicesforcac.org).

### Safe Sleep

In just 5 years in Genesee county, there were 59 infant deaths directly related to how and where those infants were placed to sleep. These deaths would not have happened had the children been placed in their proper sleep environments, which means that these deaths are 100% preventable. So why are sleep-related infant deaths the leading cause of death of infants less than one-year-old? The Safe Sleep Coalition of Genesee County, under the umbrella of the Local Leadership Group, seeks to educate the community about the rules of safe sleep. It's as easy as 'ABC'...

**A- Alone** : Share the room and not the bed. Baby should have a space all to themselves, completely free of toys, blankets, bumper pads, etc. Nothing in the crib at all, except the child.  
**B- Back** : Babies should be placed on their backs for sleep every time. They are less likely to choke when placed on their back.  
**C - Crib** : Babies should be placed in a crib, pack-and-play, or bassinet for every sleep; at night time, and at nap time. Never place a baby in an adult bed, on a couch or chair, or on the floor for seep.

<a href="https://forms.gle/LAzj7tX9HyTjH5Rb9" class="button is-primary" target="_blank">Request a One-on-One Training</a>

Read more on Safe Sleep [here](http://www.michigan.gov/safesleep).

### Abusive Head Trauma/PURPLE Crying

Abusive Head Trauma, formerly known as 'shaken baby syndrome' occurs when an infant is violently shaken by a much bigger, stronger caregiver. The data suggests that crying is the number one reason why children are abused. [The Period of PURPLE Crying](http://purplecrying.info/) is a research-based program that teaches parents about a developmental period in infancy where the child's crying increases. PURPLE is an acronym used to describe specific characteristics of an infant’s crying during this phase: Peak of crying, Unexpected, Resists soothing, Pain-like face, Long-lasting, and Evening. The PURPLE crying period takes place within the second month of the infant's life. Helping parents and caregivers to understand this phase is normal and teaching them strategies to deal with increased crying reduces the likelihood that their frustration could lead to a split-second decision that could have a lasting impact.

### Sex Offender Registry Training

Learn how to use the National and Statewide registry to find sex offenders in your neighborhood or people who are around your children.

[Click here for the Michigan Sex Offender Registry](http://www.michigan.gov/msp/0,4643,7-123-1878_24961---,00.html)

[Click here for the National Sex Offender Registry](https://www.nsopw.gov/?AspxAutoDetectCookieSupport=1)