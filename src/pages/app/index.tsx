import React from 'react'
import { Router } from '@reach/router'

import Layout from '../../components/layout'
import ReportAbuse from '../../components/app/report-abuse'
import Login from '../../components/app/login'
import PrivateRoute from '../../components/app/private-route'

const Private = () => {
  return (
    <Layout>
      <Router>
        <PrivateRoute path="/app/report-abuse" component={ReportAbuse} />
        <Login path="/app" />
      </Router>
    </Layout>
  )
}

export default Private
