---
title: Contact Us
template: page_with_blocks
title_background: Blue
title_background_image: "/media/sara-torda-ViUzwBK0Vrs-unsplash.jpg"
title_background_image_opacity: 0.25
blocks:
- template: block__map
  component: map
  latitude: "43.0181182"
  longitude: "-83.6835679"
  popup_content: "#### Voices for Children Advocacy Center  \n[515 East Street\nFlint,
    MI 48503](https://www.google.com/maps/place/515+East+St,+Flint,+MI+48503/@43.018537,-83.683602,16z/data=!4m5!3m4!1s0x88238218c69c6601:0xf211a626507a2df2!8m2!3d43.0185367!4d-83.6836024?hl=en-US)
    \ \n[810-238-3333](tel:+1-810-238-3333)"
- template: block__columns
  component: columns
  title: ''
  description: ''
  columns:
  - template: block__column
    component: column
    alignment: Left
    content: |-
      <h2>Visit or Call</h2> <div class="notification is-info">To report suspected child abuse, call Child Protective Services at <a href="tel:+1-855-444-3911">(855) 444-3911</a></div> <p>If you have questions about child abuse, or need more information, please call or email us. We’ll get right back to you.</p>

      <p><strong>Genesee County - Flint location</strong><br> <a href="https://goo.gl/maps/eMNDaw7fjYLec7PZ7" target="_blank">515 East Street<br>Flint, MI 48503</a><br> <a href="tel:+1-810-238-3333">(810) 238-3333</a></p>

      <p><strong>Shiawassee County- Owosso Location</strong><br> <a href="https://goo.gl/maps/ZH4ypfkbETLqJ56NA" target="_blank">1216 West Main Street<br>Owosso, MI 48867</a><br> <a href="tel:+1-989-723-5877">(989) 723-5877</a></p>
    width: ''
  - template: block__column
    component: column
    alignment: ''
    content: '<h2>Email</h2><div class="is-flex"> <iframe title="Contact Us" src="https://docs.google.com/forms/d/e/1FAIpQLScGbTsTyWKoRHwCc73uPBivlyD1PrhHHXplKyk2bsmWJQtZHg/viewform?embedded=true"
      style="width:640px; height: 800px; margin: 0 auto;" frameborder="0" marginheight="0"
      marginwidth="0">Loading…</iframe> </div>'
    width: ''
  - template: block__column
    component: column
    content: ''
    width: ''
    alignment: ''
- template: block__map
  component: map
  popup_content: "**Shiawassee County- Owosso Location**  \n[1216 West Main Street
    \ \nOwosso, MI 48503](https://goo.gl/maps/ZH4ypfkbETLqJ56NA)  \n[(989) 723-5877](tel:+1-989-723-5877)"
  latitude: "42.9981688"
  longitude: "-84.1874704"
position: Below
alt_text: ''
subtitle: ''

---
