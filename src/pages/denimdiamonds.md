---
template: page_with_blocks
title_background: Blue
title_background_image_opacity: 0.16
position: Above
title: denimdiamonds
subtitle: ''
title_background_image: "/media/joshua-eckstein-VAJEea9u6k8-unsplash.jpg"
blocks: []

---
<script> window.location = "https://secure.givelively.org/donate/robert-e-weiss-advocacy-center-for-children-and-youth/denim-diamonds-month-of-giving-for-voices-for-children"; </script>