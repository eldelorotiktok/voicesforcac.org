import React, { useEffect, useState } from 'react'
import { useStaticQuery, graphql, navigate } from 'gatsby'
import { useForm } from 'react-hook-form'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons'

const GATSBY_STRIPE_PUBLISHABLE_KEY =
  process.env.GATSBY_ACTIVE_ENV === 'production'
    ? process.env.GATSBY_STRIPE_PUBLISHABLE_KEY
    : process.env.GATSBY_STRIPE_PUBLISHABLE_KEY_TEST

import {
  Elements,
  CardElement,
  useStripe,
  useElements,
} from '@stripe/react-stripe-js'
import { loadStripe } from '@stripe/stripe-js'
import axios from 'axios'
import Backdrop from '@material-ui/core/Backdrop'
import CircularProgress from '@material-ui/core/CircularProgress'
import styled from 'styled-components'

import Layout from '../components/layout'
import PageTitle from '../components/page-title'
import SEO from '../components/seo'
// import DonateDialog from '../components/dialog/donate'
// import DonationCancellationDialog from '../components/dialog/donation-cancellation'
import TextInput from '../components/donate/text-input'
import EmailInput from '../components/donate/text-input'
import RadioInput, { RadioGroup } from '../components/donate/radio-input'
import MoneyInput from '../components/donate/money-input'
import StyledCardElement from '../components/donate/styled-card-element'
import Notification from '../components/notification'
import { breakpoints } from '../breakpoints'

const DurationRadioGroup = styled(RadioGroup)`
  align-items: stretch;
  text-align: center;
  .radio-container {
    width: calc(50% - 0.5rem);
    &:last-of-type {
      width: calc(50% - 0.5rem);
    }
    @media ${breakpoints.desktop} {
      width: calc(50% - 0.5rem);
      &:last-of-type {
        width: calc(50% - 0.5rem);
      }
    }
  }
`

const DonateButton = styled.button`
  width: 100%;
  @media ${breakpoints.desktop} {
    width: auto;
  }
`

const createCardElementOptions = () => ({
  style: {
    base: {
      iconColor: 'rgba(54, 54, 54, 0.3)',
      color: '#363636',
      fontSize: '16px',
      '::placeholder': {
        color: 'rgba(54, 54, 54, 0.3)',
      },
      ':focus': {
        iconColor: '#363636',
      },
    },
    invalid: {
      iconColor: '#FF3860',
      color: '#FF3860',
    },
  },
})

const DonatePage = () => {
  const data = useStaticQuery(graphql`
    {
      fileName: file(
        relativePath: { eq: "robert-collins-lP_FbBkMn1c-unsplash.jpg" }
      ) {
        childImageSharp {
          fluid(maxWidth: 1600) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)
  const defaultValues = {
    amount: '25',
    customAmount: '',
    duration: 'once',
    email: '',
    giveInHonorOf: false,
    giveInHonorOrMemoryOf: 'inHonorOf',
    inHonorOrMemoryOf: '',
    name: '',
  }
  // ? Combine these into a single state?
  const [notificationState, setNotificationState] = useState({
    open: false,
    message: '',
    type: '',
  })
  const [backdropOpen, setBackdropOpen] = useState(false)
  // const [dialogOpen, setDialogOpen] = useState(false)
  // const [
  //   donationCancellationDialogOpen,
  //   setDonationCancellationDialogOpen,
  // ] = useState(false)
  const [creditCardError, setCreditCardError] = useState()
  const {
    register,
    handleSubmit,
    watch,
    errors,
    getValues,
    reset,
    setValue,
  } = useForm({
    defaultValues,
    mode: 'onBlur',
  })

  const stripe = useStripe()
  const elements = useElements()

  // const handleClickOpen = () => {
  //   setDialogOpen(true)
  // }

  const handleDonate = async () => {
    setBackdropOpen(true)
    setCreditCardError(undefined)
    try {
      const card = elements.getElement(CardElement)
      const { error, paymentMethod } = await stripe.createPaymentMethod({
        type: 'card',
        card,
      })
      if (error) {
        throw error
      }
      const data = {
        ...getValues(),
        paymentMethod,
      }
      await axios.post(`/.netlify/functions/donate`, data)
      reset(defaultValues)
      card.clear()
      // setNotificationState({
      //   open: true,
      //   message: 'Your donation was successfully processed!',
      //   type: 'success',
      // })
      navigate('/thank-you')
    } catch (error) {
      if (error.type === 'validation_error') {
        setCreditCardError(error.message)
      } else {
        setNotificationState({
          open: true,
          message: 'There was an error processing your donation.',
          type: 'error',
        })
      }
    } finally {
      setBackdropOpen(false)
    }
  }

  const onSubmit = () => {
    // handleClickOpen()
    handleDonate()
  }

  // const handleClose = value => {
  //   setDialogOpen(false)
  //   if (value) {
  //     handleDonate()
  //   }
  // }

  // const openDonationCancellationDialog = () => {
  //   setDonationCancellationDialogOpen(true)
  // }

  // const handleDonationCancellationDialogClose = value => {
  //   setDonationCancellationDialogOpen(false)
  // }

  const AmazonCharityBanner = () => {
    if (typeof document === 'undefined') {
      return null
    }
    useEffect(() => {
      const iFrame = document.createElement('iframe')
      iFrame.style.display = 'none'
      iFrame.style.border = 'none'
      iFrame.width = '100%'
      iFrame.height = '256'
      iFrame.setAttribute && iFrame.setAttribute('scrolling', 'no')
      iFrame.setAttribute('frameborder', '0')
      setTimeout(function() {
        /* eslint-disable */
        const contents = iFrame.contentWindow
          ? iFrame.contentWindow
          : (iFrame.contentDocument as any)?.document
          ? (iFrame.contentDocument as any).document
          : iFrame.contentDocument
        /* eslint-enable */
        if (contents?.document) {
          contents.document.open()
          contents.document.write(
            decodeURIComponent(
              '%3Cdiv%20id%3D%22amznCharityBannerInner%22%3E%3Ca%20href%3D%22https%3A%2F%2Fsmile.amazon.com%2Fch%2F43-2031361%22%20target%3D%22blank%22%3E%3Cdiv%20class%3D%22text%22%20height%3D%22%22%3E%3Cdiv%20class%3D%22support-wrapper%22%3E%3Cdiv%20class%3D%22support%22%20style%3D%22font-size%3A%2025px%3B%20line-height%3A%2028px%3B%20margin-top%3A%201px%3B%20margin-bottom%3A%201px%3B%22%3ESupport%20%3Cspan%20id%3D%22charity-name%22%20style%3D%22display%3A%20inline-block%3B%22%3EVoices%20for%20Children%20Advocacy%20Center.%3C%2Fspan%3E%3C%2Fdiv%3E%3C%2Fdiv%3E%3Cp%20class%3D%22when-shop%22%3EWhen%20you%20shop%20at%20%3Cb%3Esmile.amazon.com%2C%3C%2Fb%3E%3C%2Fp%3E%3Cp%20class%3D%22donates%22%3EAmazon%20donates.%3C%2Fp%3E%3C%2Fdiv%3E%3C%2Fa%3E%3C%2Fdiv%3E%3Cstyle%3E%23amznCharityBannerInner%7Bbackground-image%3Aurl(https%3A%2F%2Fm.media-amazon.com%2Fimages%2FG%2F01%2Fx-locale%2Fpaladin%2Fcharitycentral%2Fbanner-background-image._CB485922134.png)%3Bwidth%3A300px%3Bheight%3A250px%3Bposition%3Arelative%7D%23amznCharityBannerInner%20a%7Bdisplay%3Ablock%3Bwidth%3A100%25%3Bheight%3A100%25%3Bposition%3Arelative%3Bcolor%3A%23000%3Btext-decoration%3Anone%7D.text%7Bposition%3Aabsolute%3Btop%3A20px%3Bleft%3A15px%3Bright%3A15px%3Bbottom%3A100px%7D.support-wrapper%7Boverflow%3Ahidden%3Bmax-height%3A86px%7D.support%7Bfont-family%3AArial%2Csans%3Bfont-weight%3A700%3Bline-height%3A28px%3Bfont-size%3A25px%3Bcolor%3A%23333%3Btext-align%3Acenter%3Bmargin%3A0%3Bpadding%3A0%3Bbackground%3A0%200%7D.when-shop%7Bfont-family%3AArial%2Csans%3Bfont-size%3A15px%3Bfont-weight%3A400%3Bline-height%3A25px%3Bcolor%3A%23333%3Btext-align%3Acenter%3Bmargin%3A0%3Bpadding%3A0%3Bbackground%3A0%200%7D.donates%7Bfont-family%3AArial%2Csans%3Bfont-size%3A15px%3Bfont-weight%3A400%3Bline-height%3A21px%3Bcolor%3A%23333%3Btext-align%3Acenter%3Bmargin%3A0%3Bpadding%3A0%3Bbackground%3A0%200%7D%3C%2Fstyle%3E'
            )
          )
          contents.document.close()
          iFrame.style.display = 'block'
        }
      })
      document.getElementById('amznCharityBanner')?.appendChild(iFrame)
    }, [])
    return (
      <div className="column is-one-third">
        <div id="amznCharityBanner" />
      </div>
    )
  }

  const watchAllValues = watch()
  const donationAmount = watchAllValues.amount || watchAllValues.customAmount
  return (
    <Layout>
      <SEO title="Donate" />
      {data && (
        <PageTitle
          title="Donate"
          title_background_image={data.fileName}
          title_background="Purple"
          title_background_image_opacity={0.25}
        />
      )}
      <section className="section page-content">
        <main className="container content-container">
          <div className="content">
            <div className="columns">
              <div className="column is-two-thirds">
                <form onSubmit={handleSubmit(onSubmit)}>
                  <div className="field">
                    <DurationRadioGroup className="control">
                      <label className="radio-container" key="durtion-once">
                        <input
                          className="radio-button"
                          type="radio"
                          name="duration"
                          value="once"
                          ref={register}
                          onChange={event => {
                            if (event.target.value) {
                              setValue(
                                'amount',
                                watchAllValues.duration === 'monthly'
                                  ? '10'
                                  : '25'
                              )
                            }
                          }}
                        />
                        <div className="radio-tile">Give Once</div>
                      </label>
                      <label className="radio-container" key="durtion-monthly">
                        <input
                          className="radio-button"
                          type="radio"
                          name="duration"
                          value="monthly"
                          ref={register}
                          onChange={event => {
                            if (event.target.value) {
                              setValue(
                                'amount',
                                watchAllValues.duration === 'monthly'
                                  ? '10'
                                  : '25'
                              )
                            }
                          }}
                        />
                        <div className="radio-tile">Give Monthly</div>
                      </label>
                    </DurationRadioGroup>
                  </div>

                  <TextInput
                    label="Name"
                    name="name"
                    placeholder="Enter your name"
                    register={register}
                    required={true}
                    error={errors.name ? 'Name is required' : undefined}
                  />

                  <EmailInput
                    label="Email"
                    name="email"
                    placeholder="Enter your email"
                    register={register}
                    required={true}
                    error={errors.email ? 'Email is required' : undefined}
                  />

                  {watchAllValues.duration === 'once' ? (
                    <RadioInput
                      label="Amount"
                      name="amount"
                      options={[
                        { value: '25', label: '$25' },
                        { value: '50', label: '$50' },
                        { value: '100', label: '$100' },
                        { value: '500', label: '$500' },
                        { value: '', label: 'Custom' },
                      ]}
                      register={register}
                      required={!watchAllValues.customAmount}
                      onChange={event => {
                        if (event.target.value) {
                          setValue('customAmount', '')
                        }
                      }}
                    />
                  ) : (
                    <RadioInput
                      label="Amount"
                      name="amount"
                      options={[
                        { value: '10', label: '$10' },
                        { value: '25', label: '$25' },
                        { value: '50', label: '$50' },
                        { value: '100', label: '$100' },
                        { value: '', label: 'Custom' },
                      ]}
                      register={register}
                      required={!watchAllValues.customAmount}
                      onChange={event => {
                        if (event.target.value) {
                          setValue('customAmount', '')
                        }
                      }}
                    />
                  )}

                  {!watchAllValues.amount && (
                    <MoneyInput
                      label="Custom amount"
                      name="customAmount"
                      placeholder="Donation amount"
                      register={register}
                      required={!watchAllValues.amount}
                      error={
                        errors.customAmount ? 'Amount is required' : undefined
                      }
                      onChange={event => {
                        if (event.target.value) {
                          setValue('amount', '')
                        }
                      }}
                    />
                  )}

                  <div className="field">
                    <div className="control">
                      <label className="checkbox">
                        <input
                          type="checkbox"
                          name="giveInHonorOf"
                          ref={register}
                          style={{ marginRight: '5px' }}
                        />
                        Give in honor or memory of someone
                      </label>
                    </div>
                  </div>

                  {watchAllValues.giveInHonorOf && (
                    <>
                      <div className="field">
                        <div className="control">
                          <label className="radio">
                            <input
                              type="radio"
                              name="giveInHonorOrMemoryOf"
                              value="inHonorOf"
                              ref={register({
                                required: Boolean(watchAllValues.giveInHonorOf),
                              })}
                              style={{ marginRight: '5px' }}
                            />
                            In honor of
                          </label>
                          <label className="radio">
                            <input
                              type="radio"
                              name="giveInHonorOrMemoryOf"
                              value="inMemoryOf"
                              ref={register({
                                required: Boolean(watchAllValues.giveInHonorOf),
                              })}
                              style={{ marginRight: '5px' }}
                            />
                            In memory of
                          </label>
                        </div>
                      </div>

                      <div className="field">
                        <div className="control has-icons-right">
                          <input
                            className={`input ${
                              errors.inHonorOrMemoryOf ? 'is-danger' : ''
                            }`}
                            name="inHonorOrMemoryOf"
                            placeholder={`In ${
                              watchAllValues.giveInHonorOrMemoryOf ===
                              'inMemoryOf'
                                ? 'memory'
                                : 'honor'
                            } of`}
                            ref={register({
                              required: Boolean(watchAllValues.giveInHonorOf),
                            })}
                          />
                          {errors.inHonorOrMemoryOf && (
                            <span className="icon is-small is-right has-text-danger">
                              <FontAwesomeIcon icon={faExclamationTriangle} />
                            </span>
                          )}
                        </div>
                        {errors.inHonorOrMemoryOf && (
                          <div className="help is-danger">
                            {`In ${
                              watchAllValues.giveInHonorOrMemoryOf ===
                              'inMemoryOf'
                                ? 'memory'
                                : 'honor'
                            } of`}{' '}
                            is required
                          </div>
                        )}
                      </div>
                    </>
                  )}

                  <div className="field">
                    <div className="label">Card information</div>
                    <div className="control">
                      <StyledCardElement options={createCardElementOptions()} />
                    </div>
                    {creditCardError && (
                      <div className="help is-danger">{creditCardError}</div>
                    )}
                  </div>

                  {watchAllValues.duration === 'monthly' && (
                    <p>
                      Your donation will be made today and each following month.
                      You may cancel or change this amount at any time.
                    </p>
                  )}

                  <div className="field">
                    <div className="control">
                      <DonateButton
                        type="submit"
                        className={`button is-primary is-large ${
                          backdropOpen ? 'is-loading' : ''
                        }`}
                      >
                        Donate ${donationAmount}
                        {watchAllValues.duration === 'monthly' && ' Monthly'}
                      </DonateButton>
                    </div>
                  </div>
                </form>
              </div>
              <AmazonCharityBanner />
              {/* <div className="column is-one-third">
                <div className="title is-4">Recurring Contributions</div>
                <p>
                  At any time, if you would like to cancel your monthly
                  contribution, contact us by filling out our cancellation form.
                </p>
                <button
                  className="button is-primary is-outlined"
                  onClick={() => openDonationCancellationDialog()}
                >
                  Reuest cancellation
                </button>
              </div> */}
            </div>
          </div>
        </main>
      </section>
      {/* <DonateDialog
        data={watchAllValues}
        open={dialogOpen}
        onClose={handleClose}
      />
      <DonationCancellationDialog
        open={donationCancellationDialogOpen}
        onClose={handleDonationCancellationDialogClose}
      /> */}
      <Notification state={{ notificationState, setNotificationState }} />
      <Backdrop open={backdropOpen} style={{ zIndex: 10000, color: 'white' }}>
        <CircularProgress color="inherit" />
      </Backdrop>
    </Layout>
  )
}

const stripePromise = loadStripe(GATSBY_STRIPE_PUBLISHABLE_KEY)

const Donate = () => {
  return (
    <Elements stripe={stripePromise}>
      <DonatePage />
    </Elements>
  )
}

export default Donate
