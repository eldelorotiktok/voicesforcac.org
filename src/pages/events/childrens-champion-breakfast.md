---
template: event
image: "/media/childrens-champion-award-logo.png"
title: Children's Champion Awards
start_date: 2022-03-24T07:30:00-04:00
end_date: 2022-03-24T10:00:00-04:00
location: Riverfront Conference Center
address: 1 Riverfront Plaza, Flint, MI 48502
address_link: https://www.google.com/maps/place/Riverfront+Conference+Center/@43.0176945,-83.6955226,17z/data=!3m1!4b1!4m5!3m4!1s0x8823821689036081:0xff0446ea78a6a185!8m2!3d43.01794!4d-83.69278
blocks:
- template: block__feature
  component: feature
  background_image_opacity: 1
  orientation: normal
  image: "/media/ccba-22.jpg"
  background: Light Blue
  title: More Information Coming Soon
  content: |-
    **Thank you to all those who joined us for the 2022 Children's Champions' Awards. Be sure to join our mailing list to stay up-to-date on the 2023 Awards, get updates on when nominations are open, and on the event details. More information will be available soon.**

    Follow us on Facebook to stay up to date!

    <!DOCTYPE html><html><div id="fb-root"></div><script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v14.0" nonce="sLVQgBMl"></script><div class="fb-page" data-href="https://www.facebook.com/voicesforcac/" data-tabs="timeline" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/voicesforcac/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/voicesforcac/">Voices  for Children Advocacy Center</a></blockquote></div></html>
- template: block__content
  component: content
  content: |-
    # 2023 Children's Champions' Awards

    2023 will be our 28th annual Children's Champions' Awards! Be sure to subscribe to our mailing list below if you've not already done so, and follow us on Facebook to stay up-to-date.

    <a href="https://www.facebook.com/voicesforcac" class="button is-primary" target="_blank">See us on Facebook</a>

    The Children’s Champion Awards were created in 1995 to recognize examples of those in the community doing good things for children. The awards are designed to distinguish and thank individuals, businesses, government agencies, and organizations for their tremendous contributions toward making children a priority in Genesee County.

    For more information about supporting this special event please call (810) 238-3333 ext. 206 or email [sarah@voicesforcac.org.](mailto:sarah@voicesforcac.org)

    <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css"> <style type="text/css"> #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; } /* Add your own Mailchimp form style overrides in your site stylesheet or in this style block. We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. _/ </style> <div id="mc_embed_signup"> <form action="https://voicesforcac.us9.list-manage.com/subscribe/post?u=30bec541a81700f9129735e1b&id=98d7ed5532" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate> <div id="mc_embed_signup_scroll"> <h2>Subscribe for Updates</h2> <div class="indicates-required"><span class="asterisk">_</span> indicates required</div> <div class="mc-field-group"> <label for="mce-EMAIL">Email Address  <span class="asterisk">*</span> </label> <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL"> </div> <div class="mc-field-group"> <label for="mce-FNAME">First Name </label> <input type="text" value="" name="FNAME" class="" id="mce-FNAME"> </div> <div class="mc-field-group"> <label for="mce-LNAME">Last Name </label> <input type="text" value="" name="LNAME" class="" id="mce-LNAME"> </div> <div id="mce-responses" class="clear"> <div class="response" id="mce-error-response" style="display:none"></div> <div class="response" id="mce-success-response" style="display:none"></div> </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups--> <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_30bec541a81700f9129735e1b_98d7ed5532" tabindex="-1" value=""></div> <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div> </div> </form> </div> <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames\[0\]='EMAIL';ftypes\[0\]='email';fnames\[1\]='FNAME';ftypes\[1\]='text';fnames\[2\]='LNAME';ftypes\[2\]='text';fnames\[3\]='ADDRESS';ftypes\[3\]='address';fnames\[4\]='PHONE';ftypes\[4\]='phone';}(jQuery));var $mcj = jQuery.noConflict(true);</script> <!--End mc_embed_signup-->
- template: block__columns
  component: columns
  title: 2022 Event Partner
  description: ''
  columns:
  - template: block__column
    component: column
    alignment: ''
    content: <img alt="United Way Logo" src="/media/United-Way.jpg" />
    width: One Third
- template: block__columns
  component: columns
  title: 2022 Breakfast Sponsor
  description: ''
  columns:
  - template: block__column
    component: column
    alignment: ''
    content: <img alt="Huntington Bank Logo" src="/media/huntington.png" />
    width: One Third
- template: block__content
  component: content
  content: "## 2022 Award Sponsors\n\n**MDT Award**  \nELGA Credit Union\n\n**Caring
    Adult**  \nMcLaren Health Plan  \nCommunity Foundation of Greater Flint\n\n**Volunteer
    Engagement to Serve Youth Award**  \nYeo & Yeo CPA and Business Consultants\n\n**Exceptional
    Business Award**  \nJPMorgan Chase\n\n**Exceptional Business Award**  \nHealth
    Alliance Plan (HAP)\n\n**Lifetime Achievement Award**  \nMcLaren Flint\n\n**Outstanding
    Youth Award**  \nGenesee Health System\n\n**Active and Healthy Leader Award**
    \ \nCrim Fitness Foundation"
- template: block__content
  component: content
  content: "## 2022 Other Sponsors\n\n**Virtual Sponsor**  \nRaymond James\n\n**Entertainment
    Sponsor**  \nBlue Cross Complete\n\n**Digital Sponsor**  \nCaudill Thornquist\n\n**Resilience
    Sponsor**  \nReCAST\n\n**Venue Sponsor**  \nUM- Flint"

---
