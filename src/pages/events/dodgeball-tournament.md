---
template: event
image: "/media/a0d_gunsnhosesdodgeballtournament002-1.jpeg"
title: Dodgeball Tournament
start_date: 2023-02-16T00:00:00-05:00
end_date: 
location: Grand Blanc High School East Campus Gymnasium
address: 12500 Holly Rd, Grand Blanc, MI 48439
address_link: https://www.google.com/maps/place/Grand+Blanc+Community+High+School+-+East+Campus/@42.9204684,-83.6249348,15z/data=!4m2!3m1!1s0x0:0x286e202a591c682?sa=X&ved=2ahUKEwishLb99rz5AhXrhIkEHTE_DQgQ_BJ6BAhhEAU
blocks:
- template: block__columns
  component: columns
  title: 2022 Guns N' Hoses - Dodgeball Tournament
  columns:
  - template: block__column
    component: column
    width: One Third
    alignment: Center
    content: "![Guns N' Hoses Dodegball Tournament Logo](/media/guns-n-hoses-logo-no-year.png)"
  - template: block__column
    component: column
    width: Two Thirds
    alignment: Left
    content: |-
      Come see first responders and multidisciplinary team members, including, Police/ Law Enforcement, Firefighters, CPS workers, Prosecutors, EMTs, and more, duke it out for the Dodgeball Championship Title supporting Voices for Children Advocacy Center's mission to serve victims of child abuse in Genesee and Shiawassee Counties. Who will take home the championship trophy & bragging rights this year!?

      **Our event date is still TBD, with more information coming soon, so be sure to check back and follow us on** [**facebook**](https://facebook.com/voicesforcac) **to stay up-to-date. This event is FREE for the whole family to come and see!** Concessions and merch will be available, benefitting Voices for Children, as well as a 50/50. Donations are not expected but deeply appreciated. Don't miss out!

      Are you a business interested in sponsoring this event? Reach out to [Angie Essenburg](mailto:angie@voicesforcac.org), (810) 238-3333 ext202 to learn more!
  description: ''
- template: block__gallery
  component: gallery
  images:
  - title: Dodgeball 1
    image: "/media/a0d_gunsnhosesdodgeballtournament002-1.jpeg"
  - title: Dodgeball 2
    image: "/media/4da_gunsnhosesdodgeballtournament005.jpeg"
  - title: Dodgeball 3
    image: "/media/5bb_gunsnhosesdodgeballtournament010-1.jpeg"
  - title: Dodgeball 4
    image: "/media/5c2_gunsnhosesdodgeballtournament009-1.jpeg"
  - title: Dodgeball 5
    image: "/media/7b9_gunsnhosesdodgeballtournament001-1.jpeg"
  - title: Dodgeball 6
    image: "/media/a9f_gunsnhosesdodgeballtournament007-1.jpeg"
  - title: Dodgeball 7
    image: "/media/dodgeball-photo-7.jpeg"
  title: ''
  description: ''

---
