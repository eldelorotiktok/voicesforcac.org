---
template: event
image: "/media/superhero-run-logo-no-year.png"
title: Superhero 5K Race & 1K Fun Run
start_date: 2022-09-17T00:00:00-04:00
end_date: 
location: ''
address: ''
address_link: ''
blocks:
- template: block__feature
  component: feature
  background_image_opacity: '1'
  orientation: normal
  featured_content: ''
  image: "/media/superhero-run-logo-no-year.png"
  title: Become a Super-Sponsor!
  content: "Take part in one of the most family-friendly events in Flint and get your
    business or agency exposure. Please contact (810) 238-3333 ext. 206 or [sarah@voicesforcac.org](mailto:sarah@voicesforcac.org)
    if you are interested in becoming a sponsor of the 2022 event. \n\n<a href=\"https://voicesforcac.org/media/vfc-superhero5k-sponsor-guide2022.pdf\"
    class=\"button is-primary\" target=\"_blank\">Sponsorship Guide</a>"
  background: Light Blue
  background_image: ''
- template: block__content
  component: content
  content: |-
    #### **So what _is_ a virtual race?**

    It is a flexible, non-competitive, 5K or 1K that you can do wherever you are and however you want. You can bike, swim, run, walk, or fly. There's no specific location, route, or time. Just do your superhero best between on or leading up to September 17th then post about your run on social media to share the event as far as possible.

    #### **Registration**

    Registration will open soon! Registration on the day of the event will be an additional $10.

    * **Virtual/ In-Person 5K Run & 5K Walk**
      * $30
    * **Virtual/ In-Person 1K Fun Run**
      * $25

    **What do I get for my registration fee?**

    You get to play a critical role in helping local child survivors of human trafficking, sexual abuse, physical abuse, and neglect. That's the most important thing. If you register for the virtual event you will get a finisher medal, and race-bib, for each registration. In-person registration will include the same but you will get your medal at the finish line at the event. In-person participants in the 5K Run & Walk also have placement medals by gender and age groups.

    You can add race t-shirts to your order for an additional charge at the time you register. There are adult and children sizes available! Adults' t-shirts are available in S - 4XL, $10 -$12, and children’s shirts YS-YL for $10. **Shirts are only available for purchase until 9/1/2021 so act fast!**

    **What if I don't add shirts to my registration but want them later?**

    Shirts and other merch are available to purchase in our [store](https://shop.voicesforcac.org/), but printing and shipping times vary and we can't guarantee any product to arrive before the event.

    #### **Packet Pick Up**

    Beat those lines the morning of the event by getting your race packet early. Included will be your race number/ run-bib to wear on your in-person or virtual run, as well as any T-shirts you've purchased. If you selected virtual you will also be given your participation medal. Be sure to share photos and videos to spread the word!

    **Friday, September 16 @ Complete Runner**

    **10:00 - 11:00 AM & 2:30 - 6:30 PM**

    [915 S. Dort Hwy., Flint, MI 48503](https://www.google.com/maps/place/915+S+Dort+Hwy,+Flint,+MI+48503/data=!4m2!3m1!1s0x882381c041b1368b:0xdf978f74fb1c02c9?sa=X&ved=2ahUKEwi5pPmEo8znAhUUQ80KHSqJBFwQ8gEwAHoECAsQAQ)

    ### **What to know about the event:**

    #### **Dress & Share**

    We really encourage you to join in the fun and dress as your favorite superhero! It's a lot of fun to see everyone decked out and ready to save the day. We invite you and your group/family/coworkers, to dress up and share photos and videos of you participating in the virtual or in-person events. Share your photos by tagging @voicesforcac on Facebook or use #SuperHeroFlint2022 on Instagram. We look forward to seeing your photos and videos, whether or not you dress up (but...dress up, why not)! See our[ Facebook event](https://voicesforcac.us9.list-manage.com/track/click?u=30bec541a81700f9129735e1b&id=b6dbeae503&e=1141c81e9a) page as well to see more! Sharing your photos and videos is a great way to spread the message about helping our community's most vulnerable.

    #### **How do I get my t-shirt?**

    **If you purchased a t-shirt**, your shirt will be available with package pickup at Complete Runner located at 915 Dort Hwy, Flint, MI 48503 **on our packet pickup date (9/16).** You can also choose to have your t-shirt/ race packet shipped to you for an additional charge. This is only available until 9/1/22. Select the “Shipping & Handling” add-on to your registration. You must purchase this option when you register. If you have already registered but need your packet mailed to you, please contact [ethan@voicesforcac.org.](mailto:ethan@voicesforcac.org)

    #### **Parking**

    Parking is available in a few different areas.[ The Harrison Street Parking Ramp](https://www.google.com/maps/place/Harrison+Street+Parking+Ramp/@43.0175687,-83.690719,15z/data=!4m2!3m1!1s0x0:0x89d1a0ff44a95f5b?sa=X&ved=2ahUKEwiNkqS72O3yAhUqAp0JHU4bD8oQ_BIwG3oECFQQBQ), Lot T, & Pavillion Parking, are all available to participants. The Flint Farmers' Market parking is reserved only for market patrons and cannot be used for event parking.

    #### **Racecourse**

    Our racecourse will be the same as previous events if you've joined us before. Check our racecourse map[ here](https://drive.google.com/file/d/1AY2jWOpAhGnqnMuaxve_DBfq2VGWZ2f3/view?usp=sharing). We will be kicking off on East 1st Street, between the Flint Farmers' Market and Wilson Park on UofM's campus. The course will take us through Mott Comunity College, Applewood Estate, and the Flint Cultural Center.

    #### **Severe Weather Policy**

    The safety of our athletes, spectators, and volunteers is our primary concern. In the event of inclement weather, every attempt will be made to preserve the safety of everyone involved, and complete the race as scheduled.

    In the event of rain, the event will not be canceled due to rain alone. Race officials reserve the right to delay or shorten the race based on heavy rain. In the event of severe weather (eg: lightning, tornado, high winds) the event may be delayed as race officials consult public safety officers. If there is more than a reasonable delay, or if conditions persist, race officials reserve the right to shorten or cancel the race. Participants should seek safe shelter should severe weather develop during the race.

    Please check our website, Facebook Page, and your email before the race for any changes or cancellations. If you are already at the event, listen to the announcer about any weather changes that may occur. Entry fees will not be refunded in the event the race is canceled. Entry fees will not be credited to future events.
- template: block__columns
  component: columns
  columns:
  - template: block__column
    component: column
    width: One Half
    content: |-
      # Sponsors

      ## 5K Presenting Title Sponsor

      ![GHS - Genesee Health System - Hope and health in the community](/media/genesee-health-system-outstanding-youth.png)

      ## 1K Presenting Title Sponsor

      # _Kommareddi Family Fund_

      ### 5K Medal Sponsor

      ![](/media/mclaren-flint.png)

      ### 1K Medal Sponsor

      ![GHS - Genesee Health System - Hope and Health in the Community](/media/genesee-health-system-outstanding-youth.png)

      ### T-Shirt Sponsor

      ![ELGA Credit Union](/media/elga-cu-mdt-awards.png)

      #### Volunteer T-Shirt Sponsor

      **_Total Benefit Solutions_**

      #### Finish Photo Sponsor

      **_Health Plan Alliance (HAP)_**

      #### Finish Line Banner

      **_Police Cheif's Association_**

      #### Finish Line Sponsor

      **_Meijer Store #29_**

      #### Race Big Bottom

      **_Pioneer State Mutual_**

      #### Mile Marker

      **_Molina Healthcare_**
    alignment: Left
  title: ''
  description: ''
- template: block__gallery
  component: gallery
  images:
  - title: Superhero8
    image: "/media/59022895_2074035562719315_4482258471747584000_o.jpg"
  - title: Superhero7
    image: "/media/58787500_2072350142887857_2369895544421089280_o.jpg"
  - title: Superhero6
    image: "/media/58420263_2074035556052649_6417019974488948736_o.jpg"
  - title: Superhero5
    image: "/media/57251147_2074035546052650_5435122474175954944_o.jpg"
  - title: Superhero4
    image: "/media/50860326_1938555609600645_5665261502093852672_o.jpg"
  - title: Superhero3
    image: "/media/50790008_1938555639600642_2263191213710508032_o.jpg"
  - title: Superhero2
    image: "/media/50739411_1938555622933977_7278724898082521088_n.jpg"
  - title: Superhero1
    image: "/media/50639539_1938555636267309_8192811358782226432_o.jpg"
  title: ''
  description: ''

---
# **2022 Superhero 5K Race & 1K Fun Run**

#### **It's our 10th Annual Superhero Run!**

#### **Saturday, September 17, 2022. Register today!**

<a href="https://runsignup.com/Race/MI/Flint/FlintSuperhero" class="button is-primary" target="_blank">Register Here</a>

Your registration for the Superhero 5k or 1k Fun Run goes directly to help child survivors of human trafficking, abuse, and neglect by providing them with hope, help, and healing. Voices for Children Advocacy Center serves the Flint Area, and Genesee, and Shiawassee Counties. All funds raised will benefit children who are survivors of human trafficking, abuse, or neglect.

**The Superhero 5K Race & 1K Fun Run will have both in-person and virtual registration options. Scroll below to find information on sponsorships, frequently asked questions, and more on the event.**