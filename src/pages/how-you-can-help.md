---
title: How You Can Help
template: blocks
blocks:
- template: block__cta
  component: cta
  background: Purple
  background_image: "/media/moses-vega-_YfoApRxd4I-unsplash.jpg"
  background_image_opacity: '0.25'
  button:
    url: "/donate"
    text: Give Today
  title: Donate
  subtitle: Children need fearless, compassionate adults who will protect them from
    the perils of abuse and neglect. Thank you for your support.
- template: block__feature
  component: feature
  background: Blue
  orientation: reverse
  title: Volunteer Opportunities
  content: <ul> <li>Court Appointed Special Advocates</li> <li>Family Hosts</li> <li>Internships</li>
    <li>Office / Clerical</li> <li>Beautification / Gardening</li> <li>Ambassadors</li>
    <li>Tour Guides</li> </ul> <p> <a href="https://goo.gl/forms/e2KnMKvwUVwATTU33"
    class="button is-white is-outlined">Apply Now</a> </p>
  image: "/media/IMG_5193.jpeg"
  button:
    url: "/donate"
    text: Give Today
  featured_content: ''
  background_image: ''
  background_image_opacity: 
- template: block__columns
  component: columns
  title: Pantry of Love
  columns:
  - template: block__column
    component: column
    width: One Third
    alignment: Center
    content: '![Pantry of Love: providing personal care items for children in need](/media/pantry-of-love.jpg
      "Pantry of Love")'
  - template: block__column
    component: column
    width: Two Thirds
    alignment: Left
    content: |-
      The Pantry of Love provides new basic essentials to children and families who have experienced trauma. At Voices for Children, more than 2000 families a year receive hope, help, and healing from child sexual abuse, physical abuse, neglect, and child human trafficking.

      The Pantry of Love is a space where these children are provided with essential basic needs items such as toiletries, clothing, a clean set of sheets, a soft blanket for comfort, and a new pair of shoes and socks. Every item in our pantry is brand new. The reason for this is that we believe these children must feel highly valued and worthy, we show this by providing them with brand new, nice items.

      **We want children to leave Voices for Children knowing they are worthy, they are valued, and they are loved. You can Help!**

      <a href="https://secure.givelively.org/donate/voicesforcac/pantry-of-love" class="button is-primary" target="_blank">Pantry of Love</a>

      If you would like to donate in-kind we could use any of the following new items:

      * Adult Underwear of all sizes
      * Body lotion
      * Chapstick
      * Clothes - all sizes & gender, especially in need of 3/4/5/6 boys sizes right now
      * Cotton Pads
      * Face wash & face lotion
      * Hair Brushes
      * Natural Hair Care
      * Pajamas - all sizes & gender
      * Qtips
      * Shoes - all sizes & gender
      * Socks
      * Tissues
      * Toilet Paper
  description: ''
alt_text: ''
position: Above
subtitle: ''
title_background: ''
title_background_image: ''
title_background_image_opacity: 

---
