---
template: page_with_blocks
title_background: Blue
title_background_image_opacity: ".25"
position: Above
title: Monthly Giving
subtitle: ''
title_background_image: "/media/joshua-eckstein-VAJEea9u6k8-unsplash.jpg"
blocks: []

---
<script> window.location = "https://secure.givelively.org/donate/voicesforcac/monthly-giving"; </script>