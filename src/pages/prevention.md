---
template: page_with_blocks
title_background: Light Blue
title_background_image_opacity: ".25"
position: Above
title: Prevention
subtitle: ''
title_background_image: "/media/joshua-eckstein-VAJEea9u6k8-unsplash.jpg"
blocks:
- template: block__cards
  component: cards
  cards:
  - image: "/media/parents.png"
    title_top: ''
    title: Parents
    subtitle: ''
    content: Prevention resources, trainings, and more for parents.
    link: "/Prevention/parents/"
  - image: "/media/kids.png"
    title_top: ''
    title: Kids
    subtitle: ''
    content: Stay Safe Online, R.O.A.R
    link: "/Prevention/kids/"
  - image: "/media/educators.png"
    title_top: ''
    title: Educators
    subtitle: ''
    content: Prevention resources, trainings, professional development, and more for
      Educators.
    link: "/Prevention/educators/"

---
_While it is important to protect children who need help today, it is equally important to protect other children from harm in the first place._ That is why Voices for Children also focuses on prevention. Prevention wears many faces and must occur in many places to be effective.

Below, select if you are a Parent, a Kid, or an Educator to learn more about our prevention services.