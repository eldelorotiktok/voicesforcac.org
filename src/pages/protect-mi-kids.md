---
title_background: Blue
title_background_image_opacity: ".25"
title: Protect MI Kids
subtitle: ''
title_background_image: "/media/kristin-brown-3kBtYrZ0-hw-unsplash.jpg"
template: page_with_blocks
position: Above
blocks:
- template: block__feature
  component: feature
  background: Light
  background_image_opacity: ".25"
  orientation: normal
  image: "/media/protect-mi-kids-facebook-v3-cacmi-logo.jpg"
  title: Kids Need Your Help!
  featured_content: ''
  content: |-
    A critical situation has arisen that, if not corrected, stands to compromise the safety and well-being of over 10,000 children in Michigan beginning on October 1st of this year.

    Michigan Division of Victim Services (the agency that oversees the Victims of Crime Act (VOCA funding) that their state funding will decrease by roughly 52% overall, creating budget gaps upwards of 79% for CACs statewide. On May 24, 2022, MDHHS posted an RFP amount of $12.5 million for CACs across the state. On August 15, MDHHS made CACMI aware that they had suddenly pulled back $6.5M of the $12.5M, awarding only $6M across the state. Official grant award notifications were pushed back from August 17th to August 24th, a mere 37 days prior to the intended cuts taking place.

    To correct this dire situation, we need your help. Join us in taking the following action:

    1. Contact Michigan Department of Health and Human Services Director Elizabeth Hertel at [hertele@michigan.gov](mailto:hertele@michigan.gov?subject=Save%20Michigan%27s%20Child%20Advocacy%20Centers!) to encourage investing an additional $7 million in the grant pool for VOCA grant CACV-2023 (bringing the total available to CACs to $13 million).
    2. Contact our [state legislators](https://traverseconnect.com/advocacy/public-policy-engagement/legislative-contacts/) to alert them to the defunding of CACs in Michigan and express your support for the safety and protection of children throughout our state.

    <a href="https://michiganlcv.org/at-the-capitol-2/find-your-elected-officials/" class="button is-primary" target="_blank">Find Your Representative/ Legislator</a>

    1. Support Voices for Children Advocacy Center with your [donation](https://www.voicesforcac.org/donate) today.

    <a href="https://www.voicesforcac.org/donate" class="button is-primary" target="_blank">Donate Today</a>

    **Thank you, together we can make a difference for children.**
  background_image: ''
- template: block__content
  component: content
  content: |-
    # LAST MINUTE, SHOCKING $6.5 MILLION REDUCTION AFFECTS MICHIGAN’S YOUNGEST CRIME VICTIMS

    LANSING (August 25, 2022)

    [Child Advocacy Centers of Michigan](https://www.cacmi.org/), the statewide voice of the 35 local nonprofit organizations that provide services to children who are the victims of child sexual and physical abuse and neglect, learned this week the details of the State of Michigan’s new plan to cut $6.5 million in federal, non-taxpayer funds, that allow “CACs” to provide forensic interviews, therapy, and other crucial, front-line services to the state’s youngest crime victims. The cuts are slated to take effect on October 1, 2022.

    Since 1984, these funds have passed through state government from the federal Victims of Crime Act (VOCA) dollars, collected from convicted criminals who are ordered to pay restitution, punitive damages, and other fines and fees. The state administers these funds via the Michigan Department of Health and Human Services’ Crime Victim Services Commission, which is charged with “promote services and supports that protect and enhance the health, safety, dignity, and rights of victims of crime across the state of Michigan.” CACs were originally told, via a request for proposal, to expect a pool of $12.5 million in funding, only to find out at the end of the process, just two days before grants were scheduled to be awarded, that the total, split among more than 30 organizations, would be just $6 million.

    “This is a critical situation that, if not corrected quickly, stands to compromise the safety and well-being of more than 10,000 Michigan children in just a few weeks,” said Julie Bird, Executive Director of Child Advocacy Centers of Michigan.

    Cutting VOCA funds for CACs will create a devastating ripple effect across Michigan that, unless corrected, will be felt for years to come, Bird added. In addition to a deep and sudden cut in services, this will delay the progress of child abuse investigations, continue to expose vulnerable children to their abusers, deprive children of necessary trauma-informed specialized mental health services, cause the closure of many rural CACs, and increase the burden on law enforcement. Survivors of abuse and their caregivers will be forced to navigate systemic barriers instead of having child-friendly, dedicated CACs to provide these critical services. With court systems still backed up from 2020 closures, the negative impact will be compounded in the largest counties.

    These cuts negatively affect funding for local CACs across the state with budget holes ranging from 19 to 79 percent. As it stands, only one CAC in the Upper Peninsula, located in Delta County, is receiving funds – just $73,000 to provide services for a population spanning thousands of square miles. “This means that many child survivors will have to drive six hours round trip to receive our critical and much-needed services. This is devastating for children in the U.P.,” said Melissa Middaugh, Executive Director of [Delta Regional CAC](https://www.deltaregionalcac.org/).

    [CARE House of Oakland County](https://www.carehouse.org/), founded in 1977 as the CAC serving that county’s 1.2 million residents, received no VOCA funding from this process, after receiving $817,000 in the current fiscal year. CARE House uses these funds to pay therapists and forensic interviewers, who work directly with children to heal from abuse and bring criminal offenders to justice. “After 20 years of receiving VOCA funding, and many years of passing audits with high marks, including most recently in August of 2021, we are surprised and disappointed to suddenly be told our funding through this program is cut to zero,” said Blythe Tyler, CEO of CARE House of Oakland County.

    CACs across the state are calling on supporters and elected officials to restore this crucial funding via any possible option, before services that support the safety and protection of children will have to be cut at the local community level.
- template: block__gallery
  component: gallery
  images:
  - title: Social Media Advocacy - Funding Crisis 2022 thread image 1
    image: "/media/funding-reduction-social-media-advocacy-for-child-advocacy-centers.jpg"
  title: ''
  description: ''

---
