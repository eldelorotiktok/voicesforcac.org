---
title: Crime Victims Compensation
template: page_with_blocks
title_background: Red
title_background_image: "/media/kristin-brown-3kBtYrZ0-hw-unsplash.jpg"
title_background_image_opacity: 0.25
position: Below
blocks: []
subtitle: ''

---
The Crime Victims' Compensation program is available to victims of violent crimes, including child physical and sexual abuse. Crime Victims' Compensation reimburses child victims' non-offending caregivers for many expenses related to a crime, including out-of-pocket counseling costs, and medical bills. For more information or to schedule an appointment to apply for compensation please contact our Family Advocates below. You can learn more at [https://michigan.gov/crimevictim](https://michigan.gov/crimevictim "https://michigan.gov/crimevictim")

**Genesee County**

**Amanda Richard, LMSW**  
[amanda@voicesforcac.org](mailto:amanda@voicesforcac.org)  
(810) 238-3333 ext. 217

**Ethan Hibbard**  
[ethan@voicesforcac.org](mailto:ethan@voicesforcac.org)  
(810) 238-3333 ext. 205

**Shiawassee County**

**Jaclyn Gregoricka**  
[jaclyn@voicesforcac.org](mailto:jaclyn@voicesforcac.org "Jaclyn Gregoricka")  
(810) 238-3333 ext. 224

## Brochure & Application

English: [Brochure](/media/CVSCBrochureJune2011_358764_7.pdf) | [Application](https://www.michigan.gov/documents/mdhhs/Crime_Victim_Compensation_Application_734724_7.pdf)  
Spanish: [Folleto](/media/SPANISH_COMP_BROCHURE_rev1_361880_7.pdf) | [Aplicación](/media/Spanish-CVC-Application.pdf)  
Arabic: [كراسة](/media/Arabic-CVC-Application.pdf) | [الوضعية](/media/Arabic-CVC-Brochure.pdf)