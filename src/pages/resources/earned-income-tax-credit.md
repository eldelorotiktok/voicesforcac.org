---
template: page_with_blocks
title_background: Red
title_background_image_opacity: ".25"
position: Above
title: Earned Income Tax Credit
subtitle: ''
title_background_image: "/media/kristin-brown-3kBtYrZ0-hw-unsplash.jpg"
blocks:
- template: block__gallery
  component: gallery
  images:
  - title: EITC English
    image: "/media/genesee-shiawassee-english-flyer-fy22.jpg"
  - title: FAQ EITC
    image: "/media/genesee_flyer_revised_page2-1.jpg"
  - image: "/media/genesee-shiawassee-spanish-flyer-fy22.jpg"
    title: EITC Spanish
  - image: "/media/genesee-shiawassee-spanish-flyer-fy22-more.jpg"
    title: EITC Spanish More info
  - title: Don't Miss Out EITC
    image: "/media/genesee_flyer_revised_page1-1.jpg"
  title: ''
  description: ''

---
### Did You Know?  
You could receive over:  
$3500 if you have 1 child  
$5900 if you have 2 children  
$6600 if you have 3 or more children

## What is Earned Income Tax Credit (EITC)?

Earned Income Tax Credit, (EITC), is an important benefit providing extra money to working families. To claim EITC, you need to meet certain requirements and file a tax return, even if you have no other filing requirement or owe no tax.

## Are you Eligible for EITC?

Answer questions using the [EITC Assistant on irs.gov](https://www.irs.gov/credits-deductions/individuals/earned-income-tax-credit/use-the-eitc-assistant). **A VITA (Volunteer Income Tax Assistance) volunteer can help you file for EITC for free!**

<a href="https://www.irs.gov/credits-deductions/individuals/earned-income-tax-credit/use-the-eitc-assistant" class="button is-primary" target="_blank">Learn More or Apply Today</a>