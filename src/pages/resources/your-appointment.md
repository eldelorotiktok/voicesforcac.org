---
template: page_with_blocks
title_background: Red
title_background_image_opacity: ".25"
position: Above
title: Your Appointment
subtitle: ''
title_background_image: "/media/kristin-brown-3kBtYrZ0-hw-unsplash.jpg"
blocks:
- template: block__columns
  component: columns
  columns:
  - template: block__column
    component: column
    content: "![Our Family Room](/media/social-story-family-room.jpg)"
    width: One Half
    alignment: Center
  - template: block__column
    component: column
    width: One Half
    alignment: Left
    content: "**Genesee County**\n\n**Amanda Richard, LLMSW**  \n[amanda@voicesforcac.org](mailto:amanda@voicesforcac.org)
      \ \n(810) 238-3333 ext. 217\n\n**Ethan Hibbard**  \n[ethan@voicesforcac.org](mailto:ethan@voicesforcac.org)
      \ \n(810) 238-3333 ext. 205\n\n**Shiawassee County**\n\n**Angel Lanning**  \n[angel@voicesforcac.org](mailto:angel@voicesforcac.org
      \"Angel Lanning\")  \n(810) 238-3333 ext. 224"
  title: ''
  description: ''

---
Before your appointment, please take a moment to complete our [Social History & Response form](https://docs.google.com/forms/d/e/1FAIpQLSdaUpMe6mJTAWHPapkTegMCvTdsUDEhy0sQS7FXCd3QRls-YQ/viewform) so we can better serve you. Please reach our Family Advocates any time with questions, concerns, or needs for assistance.

<a href="https://docs.google.com/forms/d/e/1FAIpQLSdaUpMe6mJTAWHPapkTegMCvTdsUDEhy0sQS7FXCd3QRls-YQ/viewform" class="button is-primary" target="_blank">Social History & Response Form</a>

## What you can expect during your visit as we do our best to respond to the threat of COVID-19.

Your safety is our priority. Voices for Children is taking several safety measures in order to protect families and staff, and slow the spread of COVID-19 in our community. If you are coming in for a forensic interview, here is what you can expect:

1\.) When you arrive, please call our center to let the staff know you have arrived and are in the parking lot. We ask that you remain in your vehicle until directed to ensure another family is not also in the building and to give staff time if needed to disinfect any surfaces for your safety.

2\.) We ask that everyone attending wear a mask that covers their nose and mouth. Staff also will be wearing masks when meeting with your family. Masks will be provided as needed.

Below you will find the contact information for our Family Advocates.

**Thank you for your understanding and compliance with these safety measures. Together we can prevent the spread of COVID-19 in our community.** [**Click here to learn more about our services including the forensic interview**](https://www.voicesforcac.org/what-we-do/services-for-children)**, or speak with our Family Advocates today.**