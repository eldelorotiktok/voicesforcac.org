---
title: What We Do
template: page_with_blocks
position: Below
title_background: Light Blue
title_background_image: "/media/joshua-eckstein-VAJEea9u6k8-unsplash.jpg"
title_background_image_opacity: 0.25
blocks:
- template: block__cards
  component: cards
  cards:
  - title_top: ''
    title: Services for Children
    subtitle: ''
    content: Interview & exam services, family advocacy, counseling services, and
      how you can help your child
    link: "/what-we-do/services-for-children"
    image: ''
  - image: ''
    title_top: ''
    title: Child & Family Therapy
    subtitle: ''
    content: Therapy services with multiple models of counseling to help your child
      heal from trauma.
    link: "/what-we-do/child-family-therapy"
  - title_top: ''
    title: Court Appointed Special Advocates (CASA)
    subtitle: ''
    content: What CASA is, why it's important, how it works, and how to get involved
    link: "/what-we-do/court-appointed-special-advocates"
    image: ''
  - title_top: ''
    title: Prevention
    subtitle: ''
    content: Safety classes & training, parent education, safe sleep, local leadership,
      and school-based programs
    link: "/prevention/"
    image: ''
  - image: ''
    title_top: ''
    subtitle: ''
    link: "/training/"
    title: Training
    content: See our training calendar, pre-register for a webinar, reach out to us
      on how we can collaborate.
alt_text: ''
subtitle: ''

---
