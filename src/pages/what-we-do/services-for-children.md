---
title: Services for Children
template: page_with_blocks
position: Below
title_background: Light Blue
title_background_image: "/media/joshua-eckstein-VAJEea9u6k8-unsplash.jpg"
title_background_image_opacity: 0.25
blocks: []
subtitle: ''

---
## Interview & Exam Services

Voices for Children Advocacy Center (formerly Weiss Child Advocacy Center) provides a place for abused children to tell their stories in a safe, friendly environment, with a single certified trained staff that can compassionately listen, examine, and ask all the necessary questions. Children and their non-offending family members meet with our Family Advocate and Forensic Interviewer to answer any questions they have. The Interviewer builds a relationship with the child to let them know that they are in a safe place and can tell their story. Behind the scenes, our staff, Law Enforcement, and Child Protective Services discuss a plan of action to keep the child and their family safe. The Family Advocate works with the family to navigate  court proceedings, assess for and assist in addressing needs, refer to community supports and therapy, and provide ongoing support. Because our facility is so home-like, we provide medical exams onsite by specially-trained medical personnel so they are less traumatic and stressful to children.

## Multidisciplinary Team

Voices for Children Advocacy Center serves as the “hub” to the Multi-Disciplinary Team (MDT), a skilled team of professionals comprised of members of many organizations including law enforcement, social services, prosecution, mental health, medical, and victim advocacy personnel. The MDT work together in the investigation, treatment, management, and prosecution of child abuse, & neglect cases.

## Family Advocacy

Family advocacy at Voices for Children Advocacy Center provides direct support to child victim and their non-offending caregiver(s). The purpose is to support the child and the caregiver in this time of crisis. Family advocates provide education about child abuse and help the family find any other resources they may need.

Advocacy and support are provided to the non-offending caregiver and the child by all of our staff. The first person the family meets is the Voices for Children staff who talks to the caregiver about what will happen during the forensic interview, what happens after the interview, helps them fill out the necessary paperwork and explains the Victim Compensation Program.

The multi-disciplinary team (MDT) talks to the caregivers after the interview to again explain what will happen next and answer any questions they may have. Our Family Advocates will help the family find counseling and support them through the legal process (including helping the child understand what happens in court and if they have to testify).

## My child has an interview – What do I need to know?

Voices for Children is carefully, & compassionately designed to be a warm, child-friendly environment. It is a safe and comfortable place for children and caregivers. When you arrive for your appointment, one of the first people you may meet is a member or the Voices for Children staff or a volunteer. You and your child will be offered snacks and a beverage and you will be shown to the center’s living room. The living room is full of child-friendly activities to make you comfortable until it is time for the interview to begin.We have ample space if you need to bring your other children or family members with you. Please be aware alleged offenders are NOT allowed in the building or on the premises. You will have an opportunity to ask questions of the law enforcement officer, Child Protective Services worker and Voices for Children staff before your child is interviewed. During this time you may also be asked if you feel comfortable with dogs, and if you would like Daphne to be part of the interview process.

### The Forensic Interview

When it is time for your child’s interview, the interviewer will be introduced. After introductions have been made, your child will go to the interview room with the interviewer. Please be aware that no one is permitted in the interview room during the interview except for your child and the interviewer. This is an important legal precaution.

The interview is conducted as a normal, child-friendly conversation. A highly-trained and experienced forensic interviewer will be speaking casually, in a way that best meets your child’s individual needs. During the interview, the investigation team assigned to the case will observe the interview and gather information from a separate room using video equipment. The recording made during the interview is used as evidence in the investigation but does not necessarily take the place of testifying in court. Cell phones are NOT allowed in the interview room. If your child has a cell phone, please ask them to leave it at home or with you during the interview.

When the interview is complete the MDT will meet with you regarding the next steps in the investigation. You can ask questions the team about the investigation and about what you can do to help your child. Pursuant to Michigan law you will not be permitted to watch the video of the interview. Only the MDT working on the investigation are able to view the video.

### The Medical Exam

The investigation team may request a specialized medical exam. If they do not request an exam, you can ask that your child receive one, or take your child to your family physician. Even if your child has already been examined by another doctor, it is common for the team to request an exam.

Medical exams are conducted by a doctor or SANE (Sexual Assault Nurse Examiner) who are trained to work with abused children. A medical exam is an important part of the investigation because it helps to ensure your child’s physical health going forward.

The medical staff takes great care to make sure the exam is not painful or uncomfortable for your child. The friendly and inviting environment at Voices for Children helps children feel more comfortable and ease any nervousness associated with the exam. It also allows your child to be interviewed and examined in the same familiar place.

Laboratory testing will be conducted to ensure that your child was not infected with a sexually transmitted infection or disease. Except for the usual discomfort associated with a blood test, this is not painful to your child. Some children who have been abused may have normal exam results. A negative exam result does not necessarily mean abuse did not occur.

The doctors can answer any questions you may have about the exam and what they found. Feel free to talk to the doctors openly about any concerns you may have.

## How you can help your child? (Before the Interview)

### Talking To Your Child

Do not unload your feelings about the abuse or investigation on your child. Children often misinterpret an adult’s distress and may blame themselves or shut down. This interferes with their ability to heal. Instead, open up to a trusted family member, friend or counselor.

Some advice for talking with your child:

* Do not press them for information about what they said in the interview. Follow your child’s lead when it comes to discussing what happened.
* Remain calm when talking with your child.
* Assure your child that you believe and support them.
* Let your child know you will keep him or her safe.
* Spend quality time with your child. Let him or her know that they can express any thoughts and emotions to you any time.
* Let your child know that you are proud of him or her.
* Do not promise a particular legal outcome.
* Remain consistent in daily routines as well as discipline. This will help your child feel more secure and go “back to normal.”

## How you can help

### Support your Child

The best way for you to help the investigation is to support your child. Being supportive may be difficult for you, especially if you were close to the alleged offender. Ask your child about how they feel and don’t stop loving them. Talk with all of the children in your care about abuse. Teach them about appropriate and inappropriate touching with adults and other children. Encourage your child to live as normally as possible by participating in activities with other kids.

### Fully Cooperate with Investigators

You may feel that your privacy is being invaded during the course of the investigation. You may not understand why the investigators and workers need to know so many details about your life. They ask personal questions because the information can help the investigation. Cases are often damaged by information that the investigators did not know early on in the investigation. For this reason, it is very important that you fully cooperate with the investigation. Provide as many facts as you can, but do not guess answers to any questions. The investigators understand that this process may be difficult for you and they appreciate your cooperation.

### Do NOT Coach your Child

Caregivers often think that practicing what to say before the interview or court proceedings will help your child and the investigation. This is not true. The prosecutor’s office will work with your child to prepare for court as needed. Voices for Children's Child & Family Therapists are available to help orient your child to what court may be like as well. Recently, the Prosecuting Attorneys Office instituted a Special Victims Unit which uses its wealth of knowledge and experience with sexual abuse, and human trafficking cases to best meet the needs of children, youth, and families throughout the court process.

### Keep in Touch

Let the investigators know about any changes in your life as soon as possible, such as whether you have moved, any contact the offender has or attempts with your family, or any new information you have that may affect the investigation.