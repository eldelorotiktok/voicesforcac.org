---
title: Board
template: page_with_blocks
position: Above
title_background: Orange
title_background_image: "/media/mi-pham-FtZL0r4DZYk-unsplash.jpg"
title_background_image_opacity: 0.25
blocks:
- template: block__cards
  component: cards
  cards:
  - title_top: ''
    title: 'Chief Ron Wiles '
    subtitle: Chief of Grand Blanc Twp | Genesee County Chief of Police Association|
      Board Chair
    image: ''
    content: ''
    link: ''
  - title_top: ''
    title: Cheryl Sclater
    subtitle: ELGA Credit Union  | Vice Chair
    image: ''
    content: ''
    link: ''
  - image: ''
    title_top: ''
    title: Kevin Lenkart
    subtitle: Director of Public Safety Police and Fire| Vice Chair
    content: ''
    link: ''
  - image: ''
    title_top: ''
    title: Tamara Brickey
    subtitle: Genesee Co. Health Department | Secretary
    content: ''
    link: ''
  - image: ''
    title_top: ''
    title: Jeff Collier
    subtitle: J.P. Chase | Treasurer
    content: ''
    link: ''
  - title_top: ''
    title: Tamara Phillips
    subtitle: Genesee County Prosecutor's Office
    image: ''
    content: ''
    link: ''
  - title_top: ''
    title: Judge Duncan Beagle
    subtitle: 7th Judicial Circuit Court
    image: ''
    content: ''
    link: ''
  - image: ''
    title_top: ''
    title: Dr. Julie Sovis
    subtitle: Pediatrician – Memorial Healthcare
    content: ''
    link: ''
  - image: ''
    title_top: ''
    title: Alison Morrison
    subtitle: Shiawassee County DHHS
    content: ''
    link: ''
  - image: ''
    title_top: ''
    title: Crystal Wirostek
    subtitle: Shiawassee Health & Wellness
    content: ''
    link: ''
  - image: ''
    title_top: ''
    title: Prosecutor  Scott Koerner
    subtitle: Prosecutor – Shiawassee County
    content: ''
    link: ''
  - title_top: ''
    title: Angie Hendershot
    subtitle: WJRT ABC12
    image: ''
    content: ''
    link: ''
  - image: ''
    title_top: ''
    title: Courtney Herrick
    subtitle: Director – Great Start Collaborative
    content: ''
    link: ''
  - title_top: ''
    title: Prosecutor David Leyton
    subtitle: Genesee County Prosecutor
    image: ''
    content: ''
    link: ''
  - image: ''
    title_top: ''
    title: Kristina Marshall, J.D.
    subtitle: Oakland Community College
    content: ''
    link: ''
  - title_top: ''
    title: Michael Milks
    subtitle: Genesee County Department of Human Services Child Welfare Director
    image: ''
    content: ''
    link: ''
  - image: ''
    title_top: ''
    title: Justina Miller
    subtitle: 'Parent Representative '
    content: ''
    link: ''
  - title_top: ''
    title: Mattie Pearson
    subtitle: Hurley Medical Center
    image: ''
    content: ''
    link: ''
  - title_top: ''
    title: Dan Russell
    subtitle: Genesee Health System President & CEO
    image: ''
    content: ''
    link: ''
  - title_top: ''
    title: Sheriff Chris Swanson
    subtitle: Genesee County Sheriff
    image: ''
    content: ''
    link: ''
  - title_top: ''
    title: Cpt. Sondra Taylor
    subtitle: City of Flint Police Department
    image: ''
    content: ''
    link: ''
  - title_top: ''
    title: James Yake
    subtitle: GISD
    image: ''
    content: ''
    link: ''
subtitle: ''

---
Our board is comprised of civic and community leaders who are dedicated to the safety and well-being of children.