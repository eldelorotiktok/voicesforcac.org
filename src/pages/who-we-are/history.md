---
title: History
template: page_with_blocks
position: Below
title_background: Orange
title_background_image: "/media/mi-pham-FtZL0r4DZYk-unsplash.jpg"
title_background_image_opacity: 0.25
blocks: []
subtitle: ''

---
Voices for Children Advocacy Center is the result of a merger between Weiss Child Advocacy Center and Priority Children. The history of both organizations is below. Currently, Voices for Children is a child –friendly, place where child victims of abuse receive a forensic-child-focused interview, undergo medical evaluations, receive therapy services and receive support, advocacy and resources. Parents and children are able to receive the services they need and ask any questions they need. Prior to having the child advocacy center, children of abuse and their families had to navigate the different systems alone and had to retell the traumatic events of the abuse multiple times. Parents and children no longer walk through this process alone.

The center also services as an advocate and voice for children in Genesee & Shiawassee Counties to ensure children have their basic needs.

## Voices for Children Advocacy Center

Genesee County recognized the need to have a Child Advocacy Center for children who were alleged victims of child abuse or neglect. In 2003, with funding from the Ruth Mott Foundation the Child Advocacy Center of Genesee County was formed.

In 2010, the Robert E. Weiss Advocacy Center for Youth and Children (commonly known as Weiss Child Advocacy Center) was born through the merger of two well-established and highly respected child welfare agencies in Genesee County: the Consortium on Child Abuse and Neglect (C/CAN), and the Child Advocacy Center (CAC). C/CAN was established in 1974 to advocate against child abuse and neglect. C/CAN has established a variety of education and prevention programs for Genesee County to help change the child welfare system and prevent abuse. This merger brought direct services and prevention for child abuse and neglect under one roof. As of 2018, Weiss Child Advocacy Center merged also with Priority Children to form Voices for Children Advocacy Center.

Voices for Children is accredited by the National Children Alliance (NCA) and is the home to the CASA (Court Appointed Special Advocate) of Genesee County program, as well as several other programs and services.

Voices for Children serves as the “hub” to the Multi-Disciplinary Team (MDT), a skilled team of professionals comprised of members of many organizations including law enforcement, social services, prosecution, mental health, medical, and victim advocacy personnel. The MDT work together in the investigation, treatment, management, and prosecution of child abuse cases. Having a facility like Voices for Children allows the child to begin their healing immediately after the forensic interview, it allows the child to only have to recount the event once, and it allows for an effective prosecution of the offender.