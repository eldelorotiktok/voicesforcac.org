---
title: Staff
alt_text: ''
template: page_with_blocks
position: Below
title_background: Orange
title_background_image: "/media/mi-pham-FtZL0r4DZYk-unsplash.jpg"
title_background_image_opacity: 0.25
blocks:
- template: block__cards
  component: cards
  cards:
  - title_top: Genesee County
    title: Claudnyse
    subtitle: President & CEO
    image: ''
    content: ''
    link: ''
  - title_top: Genesee County
    title: Sarah
    subtitle: Vice President of Advocacy and Advancement
    image: ''
    content: ''
    link: ''
  - title_top: Genesee & Shiawassee Counties
    title: Gayle
    subtitle: MDT Coordinator
    image: ''
    content: ''
    link: ''
  - title_top: Genesee County
    title: Angie
    subtitle: Forensic Interviewer
    image: ''
    content: ''
    link: ''
  - title_top: Genesee County
    title: Maria
    subtitle: Forensic Interviewer
    image: ''
    content: ''
    link: ''
  - title_top: Genesee County
    title: Paula
    subtitle: Child and Family Therapist
    image: ''
    content: ''
    link: ''
  - title_top: Genesee County
    title: Kayla
    subtitle: Child and Family Therapist
    image: ''
    content: ''
    link: ''
  - title_top: Genesee County
    title: Autumn
    subtitle: Child & Family Therapist
    image: ''
    content: ''
    link: ''
  - image: ''
    title_top: Genesee County
    title: Sonja
    subtitle: Child & Family Therapist
    content: ''
    link: ''
  - image: ''
    title_top: Genesee County
    title: Tabitha
    subtitle: Crisis Advocate
    content: ''
    link: ''
  - title_top: Genesee County
    title: Amanda
    subtitle: Family Advocate, Child & Family Therapist
    image: ''
    content: ''
    link: ''
  - title_top: Genesee County
    title: Ethan
    subtitle: Family Advocate
    image: ''
    content: ''
    link: ''
  - title_top: Genesee County
    title: Tenesa
    subtitle: Prevention Coordinator and Forensic Interviewer
    image: ''
    content: ''
    link: ''
  - title_top: Genesee County
    title: Alyssa
    subtitle: Prevention and Advocacy Coordinator
    image: ''
    content: ''
    link: ''
  - title_top: Genesee County
    title: Candace
    subtitle: CASA Volunteer Coordinator
    image: ''
    content: ''
    link: ''
  - image: ''
    title_top: Genesee County - Americorps Flint Urban Safety and Health Corps
    title: Mya
    subtitle: Americorps Volunteer
    content: ''
    link: ''
  - title_top: Genesee County
    title: Daphne
    subtitle: Canine Advocate
    image: ''
    content: ''
    link: ''
- template: block__cards
  component: cards
  cards:
  - image: ''
    title_top: Shiawassee County
    subtitle: Vice President of CAC Services
    content: ''
    link: ''
    title: Ellen
  - image: ''
    title_top: Shiawassee County
    title: Sierra
    subtitle: Crisis Counselor
    content: ''
    link: ''
  - image: ''
    title_top: Shiawassee County
    title: Angel
    subtitle: Forensic Interviewer
    content: ''
    link: ''
  - image: ''
    title: Jackie
    subtitle: Family Advocate
    content: ''
    link: ''
    title_top: Shiawassee County
- template: block__gallery
  component: gallery
  title: ''
  description: ''
  images:
  - title: SW
    image: "/media/69468195_2473398422757495_2507858629932089344_o.jpg"
  - title: Doggie
    image: "/media/22195459_1758397157564452_3190290517197059998_n.jpg"
  - image: "/media/IMG_5193.jpeg"
    title: Group Photo
- template: block__feature
  component: feature
  background: Purple
  orientation: reverse
  title: Meet Daphne - Canine Advocate
  content: |-
    Daphne joined our staff in June 2017 from Rochester, MI where she worked with Leader Dogs for the Blind. Our friends at Canine Advocacy Program (CAP) worked with Daphne and her handlers to enhance her training and further develop her skills. In less than a month, she was ready to start working with kids in the Forensic Interviews and in court. She is a one year old Black Labrador/Golden Retriever hybrid dog who loves giving kisses, playing with her toys and finding sticks in the yard.

    Daphne was brought to Voices for Children to accompany children in Forensic Interviews, support groups, court proceedings, medical exams and other events throughout the community. Our hope with having Daphne at our facility is to reduce anxiety and bring joy to the people we work with. We really enjoy having her on staff and we hope she brings you as much joy as she has brought us.
  featured_content: <iframe title="Meet Daphne" width="560" height="315" src="https://www.youtube.com/embed/r9HYNJwjMZc"
    frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
    allowfullscreen></iframe>
  background_image: ''
  background_image_opacity: 
  image: ''
subtitle: ''

---
