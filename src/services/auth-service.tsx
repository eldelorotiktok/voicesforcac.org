import Cookies from 'js-cookie'

import * as siteConfig from '../../site-config.json'

const isBrowser = typeof window !== `undefined`

const getIsLoggedIn = () => Cookies.get('isLoggedIn')

const setIsLoggedIn = () => Cookies.set('isLoggedIn', true)

const clearIsLoggedIn = () => Cookies.remove('isLoggedIn')

export const handleLogin = ({ password }) => {
  if (!isBrowser) return false

  if (String(password) === siteConfig.private_page_password) {
    setIsLoggedIn()
    return true
  }

  return false
}

export const isLoggedIn = () => {
  if (!isBrowser) return false

  return getIsLoggedIn() === 'true'
}

export const logout = callback => {
  if (!isBrowser) return

  clearIsLoggedIn()
  callback()
}
