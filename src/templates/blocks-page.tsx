import React from 'react'
import { graphql } from 'gatsby'

import Layout from '../components/layout'
import Blocks from '../components/blocks'
import SEO from '../components/seo'

const BlocksTemplate = props => {
  const post = props.data.markdownRemark
  const { excerpt } = post

  return (
    <Layout>
      <SEO
        title={post.frontmatter.title}
        description={excerpt}
        image={
          post.frontmatter.image
            ? post.frontmatter.image.childImageSharp.fluid.src
            : null
        }
      />
      <Blocks blocks={post.frontmatter.blocks} />
    </Layout>
  )
}

export default BlocksTemplate

export const pageQuery = graphql`
  query BlockPageBySlug($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      excerpt
      html
      ...BlocksFrontmatter
    }
  }
`
