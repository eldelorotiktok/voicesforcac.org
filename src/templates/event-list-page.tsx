import React from 'react'
import { graphql } from 'gatsby'

import Layout from '../components/layout'
import { getDateString } from '../helpers/lib/get-date-string'
import Banner from '../components/blocks/banner'
import Blocks from '../components/blocks'
import Cards from '../components/blocks/cards'
import PageTitle from '../components/page-title'
import SEO from '../components/seo'

const Events = props => {
  const post = props.data.markdownRemark
  const { excerpt } = post
  const { blocks } = post.frontmatter

  const events = props.data.allMarkdownRemark.edges

  const cards = events.map(item => {
    const event = item.node
    const { title, image, location } = event.frontmatter
    const { fields } = event
    const { slug } = fields
    const title_top = getDateString(event.frontmatter)
    return {
      title,
      image,
      link: slug,
      title_top,
      subtitle: location,
    }
  })

  return (
    <Layout>
      <SEO
        title={post.frontmatter.title}
        description={excerpt}
        image={
          post.frontmatter.image
            ? post.frontmatter.image.childImageSharp.fluid.src
            : null
        }
      />
      {post.frontmatter.image && (
        <Banner
          block={{
            image: post.frontmatter.image,
            alt_text: post.frontmatter.alt_text || post.frontmatter.title,
          }}
        />
      )}
      <PageTitle
        title={post.frontmatter.title}
        subtitle={post.frontmatter.subtitle}
        title_background={post.frontmatter.title_background}
        title_background_image={post.frontmatter.title_background_image}
        title_background_image_opacity={
          post.frontmatter.title_background_image_opacity
        }
      />
      {blocks && <Blocks blocks={blocks} />}
      <section className="section page-content">
        <main className="container content-container">
          <div
            className="content"
            dangerouslySetInnerHTML={{ __html: post.html }}
          />
        </main>
        <Cards block={{ cards }} />
      </section>
    </Layout>
  )
}

export default Events

export const pageQuery = graphql`
  query EventsPageQuery($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      excerpt
      html
      ...PageWithBlocksFrontmatter
    }
    allMarkdownRemark(
      sort: { order: ASC, fields: [frontmatter___start_date] }
      filter: { fileAbsolutePath: { regex: "/(events)//" } }
    ) {
      edges {
        node {
          id
          excerpt
          html
          fields {
            slug
          }
          ...EventFrontmatter
        }
      }
    }
  }
`
