import React from 'react'
import { graphql } from 'gatsby'
import Img from 'gatsby-image'
import styled from 'styled-components'

import Layout from '../components/layout'
import { getDateString } from '../helpers/lib/get-date-string'
import Blocks from '../components/blocks'
import SEO from '../components/seo'

const TitleTop = ({ className, children }) => {
  return <small className={className}>{children}</small>
}

const StyledTitleTop = styled(TitleTop)`
  display: block;
  margin-bottom: 1rem;
`

const AddressText = styled.small`
  display: block;
  margin-top: 0.25rem;
`

const Address = ({ address, addressLink }) => {
  if (addressLink) {
    return (
      <AddressText>
        <a href={addressLink} target="_blank" rel="noopener noreferrer">
          {address}
        </a>
      </AddressText>
    )
  }
  return <AddressText>{address}</AddressText>
}

const EventTemplate = props => {
  const post = props.data.markdownRemark

  const dateString = getDateString(post.frontmatter)
  const { excerpt, frontmatter } = post
  const { address, address_link, blocks, image, location, title } = frontmatter

  return (
    <Layout>
      <SEO
        title={title}
        description={excerpt}
        image={image ? image.childImageSharp.fluid.src : null}
      />
      <section className="section page-content">
        <div className="container">
          <div className="columns is-vcentered">
            {image && (
              <div className="column is-4">
                <figure className="image">
                  <Img fluid={image.childImageSharp.fluid} />
                </figure>
              </div>
            )}
            <div className="column">
              <StyledTitleTop>{dateString}</StyledTitleTop>
              <h1 className="title is-2">{title}</h1>
              <h2 className="subtitle is-4">
                <div>{location}</div>
                <Address address={address} addressLink={address_link} />
              </h2>
            </div>
          </div>
          <hr />
        </div>
        {post.html && (
          <main className="container content-container">
            <div
              className="content"
              dangerouslySetInnerHTML={{ __html: post.html }}
            />
          </main>
        )}
      </section>
      {blocks && <Blocks blocks={blocks} />}
    </Layout>
  )
}

export default EventTemplate

export const pageQuery = graphql`
  query EventPageBySlug($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      excerpt
      html
      ...EventFrontmatter
    }
  }
`
