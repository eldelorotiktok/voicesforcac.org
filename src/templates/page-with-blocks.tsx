import React from 'react'
import { graphql } from 'gatsby'

import Layout from '../components/layout'
import Banner from '../components/blocks/banner'
import Blocks from '../components/blocks'
import PageTitle from '../components/page-title'
import SEO from '../components/seo'

const SingleBlocksTemplate = props => {
  const post = props.data.markdownRemark
  const { excerpt } = post
  const { blocks, position } = post.frontmatter
  const isBelow = position === 'Below'

  return (
    <Layout>
      <SEO
        title={post.frontmatter.title}
        description={excerpt}
        image={
          post.frontmatter.image
            ? post.frontmatter.image.childImageSharp.fluid.src
            : null
        }
      />
      {post.frontmatter.image && (
        <Banner
          block={{
            image: post.frontmatter.image,
            alt_text: post.frontmatter.alt_text || post.frontmatter.title,
          }}
        />
      )}
      <PageTitle
        title={post.frontmatter.title}
        subtitle={post.frontmatter.subtitle}
        title_background={post.frontmatter.title_background}
        title_background_image={post.frontmatter.title_background_image}
        title_background_image_opacity={
          post.frontmatter.title_background_image_opacity
        }
      />
      {isBelow && blocks && <Blocks blocks={blocks} />}
      {post.html && (
        <section className="section page-content">
          <main className="container content-container">
            <div
              className="content"
              dangerouslySetInnerHTML={{ __html: post.html }}
            />
          </main>
        </section>
      )}
      {!isBelow && blocks && <Blocks blocks={blocks} />}
    </Layout>
  )
}

export default SingleBlocksTemplate

export const pageQuery = graphql`
  query PageWithBlocksBySlug($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      excerpt
      html
      ...PageWithBlocksFrontmatter
    }
  }
`
