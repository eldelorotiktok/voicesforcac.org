import React from 'react'
import { graphql } from 'gatsby'

import Layout from '../components/layout'
import Banner from '../components/blocks/banner'
import PageTitle from '../components/page-title'
import SEO from '../components/seo'

const SingleTemplate = props => {
  const post = props.data.markdownRemark
  const { excerpt } = post

  return (
    <Layout>
      <SEO
        title={post.frontmatter.title}
        description={excerpt}
        image={
          post.frontmatter.image
            ? post.frontmatter.image.childImageSharp.fluid.src
            : null
        }
      />
      {post.frontmatter.image && (
        <Banner
          block={{
            image: post.frontmatter.image,
            alt_text: post.frontmatter.alt_text || post.frontmatter.title,
          }}
        />
      )}
      <PageTitle
        title={post.frontmatter.title}
        subtitle={post.frontmatter.subtitle}
        title_background={post.frontmatter.title_background}
        title_background_image={post.frontmatter.title_background_image}
        title_background_image_opacity={
          post.frontmatter.title_background_image_opacity
        }
      />
      <section className="section page-content">
        <main className="container content-container">
          <div
            className="content"
            dangerouslySetInnerHTML={{ __html: post.html }}
          />
        </main>
      </section>
    </Layout>
  )
}

export default SingleTemplate

export const pageQuery = graphql`
  query SinglePageBySlug($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      excerpt
      html
      ...Frontmatter
    }
  }
`
