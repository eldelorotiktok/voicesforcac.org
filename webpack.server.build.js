const webpack = require('webpack')

// This is done to have Netlify use different environment variables for deployment environments
const env = process.env
env.STRIPE_SECRET_KEY =
  env.NODE_ENV === 'production'
    ? env.STRIPE_SECRET_KEY
    : env.STRIPE_SECRET_KEY_TEST

module.exports = {
  module: {
    rules: [
      {
        test: /\.(m?js|ts)?$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              '@babel/preset-typescript',
              [
                '@babel/preset-env',
                {
                  targets: {
                    node: true,
                  },
                },
              ],
            ],
            plugins: [
              '@babel/plugin-proposal-class-properties',
              '@babel/plugin-transform-object-assign',
              '@babel/plugin-proposal-object-rest-spread',
            ],
          },
        },
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': JSON.stringify(env),
    }),
  ],
}
